<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel{
    protected $commands = [];

    protected function schedule(Schedule $schedule){
        //$schedule->command('ban:delete-expired')->daily();
        $schedule->command('sitemap:generate')->daily();
        $schedule->command('currency:rate')->evenInMaintenanceMode()->dailyAt('09:15')->timezone('Europe/Istanbul')->emailOutputTo('bayram4ik@gmail.com');
    }

    protected function commands(){
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
