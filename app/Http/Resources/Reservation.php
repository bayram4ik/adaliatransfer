<?php namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Reservation extends Resource
{
    private $statuses = [
        'canceled'  => 'Iptal',
        'pending'   => 'Gerceklesmemis',
        'completed' => 'Gerceklesmis'
    ];

    public function toArray($request)
    {
        return [
            'id'                     => $this->id,
            'agency'                 => $this->agency ? $this->agency->name : null,
            'agency_id'              => $this->agency_id,
            'pickup_location'        => $this->pickup_location ? $this->pickup_location->name : null,
            'pickup_location_parent' => $this->pickup_location->parent ? $this->pickup_location->parent->name : null,
            'pickup_location_id'     => $this->pickup_location_id,
            'dropoff_location'       => $this->dropoff_location ? $this->dropoff_location->name : null,
            'dropoff_location_parent' => $this->dropoff_location->parent ? $this->dropoff_location->parent->name : null,
            'dropoff_location_id'    => $this->dropoff_location_id,
            'pickup_address'         => $this->pickup_address,
            'dropoff_address'        => $this->dropoff_address,
            'pickup_at'              => $this->pickup_at,
            'flight'                 => $this->flight,
            'flight_at'              => $this->flight_at,
            'return_pickup_at'       => $this->return_pickup_at,
            'return_flight'          => $this->return_flight,
            'return_flight_at'       => $this->return_flight_at,
            'adults'                 => (int)$this->adults,
            'children'               => (int)$this->children,
            'total_pax'              => (int)($this->adults+$this->childs),
            'baggages'               => (int)$this->baggages,
            'child_seats'            => (int)$this->child_seats,
            'boosters'               => (int)$this->boosters,
            'services'               => (array)$this->services,
            'total'                  => (float)$this->total,
            'discount'               => (float)$this->discount,
            'currency'               => $this->currency,
            'prepaid'                => (float)$this->prepaid,
            'paid'                   => (float)$this->paid,
            'agency_comission'       => (float)$this->agency_comission,
            'payment_currency'       => $this->payment_currency,
            'status'                 => $this->status,
            'status_name'            => $this->statuses[$this->status],
            'client_id'              => $this->client_id,
            'client'                 => $this->client ? $this->client->name : null,
            'client_name'            => $this->client_name,
            'client_phone'           => $this->client_phone,
            'client_email'           => $this->client_email,
            'client_comment'         => $this->client_comment,
            'vehicle_id'             => $this->vehicle_id,
            'vehicle'                => $this->vehicle,
            'vehicle_name'           => $this->vehicle ? $this->vehicle->name : null,
            'plate'                  => $this->plate,
            'duration'               => (int)$this->duration,
            'distance'               => (int)$this->distance,
            'comission_receipt_id'   => $this->comission_receipt_id,
            'confirmed_at'           => $this->confirmed_at,
            'accepted_at'            => $this->accepted_at,
            'rejected_at'            => $this->rejected_at,
            'canceled_at'            => $this->canceled_at,
            'created_at'             => $this->created_at,
            'updated_at'             => $this->updated_at,
            'deleted_at'             => $this->deleted_at
        ];
    }
}
