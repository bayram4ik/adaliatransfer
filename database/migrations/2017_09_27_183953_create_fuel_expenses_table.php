<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuelExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->char('vehicle_id', 10);
            $table->integer('start_km')->unsigned()
                  ->comment('In UI show how much km passed after last load, TRY/km, lt/km');
            $table->integer('end_km')->unsigned()->default(0);
            $table->decimal('lt', 8, 2);
            $table->decimal('total', 8, 2);
            $table->decimal('price', 8, 4)->storedAs('total/lt');
            $table->decimal('km_cost', 8, 4)
                  ->storedAs('CASE WHEN end_km > start_km THEN (end_km-start_km)/total ELSE 0 END');
            $table->decimal('km_lt', 8, 4)
                  ->storedAs('CASE WHEN end_km > start_km THEN (end_km-start_km)/lt ELSE 0 END');
            $table->char('currency_id', 3)->default('TRY');
            $table->timestampTz('refueled_at')->nullable();
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_expenses');
    }
}
