<?php

namespace App\Providers;
use View;
use App\Setting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\Resource;
use App;
use Cookie;
use App\Currency;
use App\ExchangeRate;
use App\Language;

class AppServiceProvider extends ServiceProvider {
    public function boot(){
        View::share('settings', Setting::with('attachments')->first());
        $currencies = Currency::where('active', true)
                              ->get()
                              ->keyBy('id');
        View::share('currencies', $currencies);
        $rates = ExchangeRate::latest()
                             ->limit(3)
                             ->select(['currency_id', 'selling'])
                             ->get()
                             ->keyBy('currency_id');
        View::share('rates', $rates);

        View::share('languages', Language::where('active', true)->get()->keyBy('id'));
        Resource::withoutWrapping();
    }
}
