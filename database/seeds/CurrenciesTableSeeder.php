<?php

use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('currencies')->delete();
        
        \DB::table('currencies')->insert(array (
            0 => 
            array (
                'id' => 'EUR',
                'name' => 'Euro',
                'symbol' => '€',
                'main' => 0,
                'active' => 1,
                'created_at' => '2017-12-12 12:12:39',
                'updated_at' => '2017-12-12 12:12:39',
            ),
            1 => 
            array (
                'id' => 'TRY',
                'name' => 'Turkish Lira',
                'symbol' => '₺',
                'main' => 1,
                'active' => 1,
                'created_at' => '2017-12-12 12:11:56',
                'updated_at' => '2017-12-12 12:11:56',
            ),
            2 => 
            array (
                'id' => 'USD',
                'name' => 'US Dollar',
                'symbol' => '$',
                'main' => 0,
                'active' => 1,
                'created_at' => '2017-12-12 12:10:58',
                'updated_at' => '2017-12-12 12:10:58',
            ),
        ));
        
        
    }
}