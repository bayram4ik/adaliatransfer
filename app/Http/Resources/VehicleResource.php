<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class VehicleResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'name_trans'        => $this->getTranslations('name') && !empty($this->getTranslations('name')) ? $this->getTranslations('name') : null,
            'pax_capacity'      => $this->pax_capacity,
            'baggage_capacity'  => $this->baggage_capacity,
            'models'            => $this->models,
            'models_trans'      => $this->getTranslations('models') && !empty($this->getTranslations('models')) ? $this->getTranslations('models') : null,
            'attachments'       => $this->whenLoaded('attachments')
        ];
    }
}
