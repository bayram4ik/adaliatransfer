<?php namespace App\Events;

//use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;

class EntryDeleted implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $path;
    public $id;

    public function __construct($path, $id){
        $this->path = $path;
        $this->id   = $id;
    }

    public function broadcastWith(){
        return [
            'path' => $this->path,
            'id'   => $this->id
        ];
    }

    public function broadcastOn(){
        return new PrivateChannel('updates');
    }
}
