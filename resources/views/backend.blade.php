<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/core.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/components.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/colors.min.css') }}" rel="stylesheet" type="text/css">
        <script src="{{ mix('js/jquery.slim.min.js') }}"></script>
        <script>
            window.user = {!! $user !!};
            window.roles = {!! $roles !!};
            window.permissions = {!! $permissions !!};
            window.settings = {!! $settings !!};
        </script>
    </head>
<body>
    <div id="app"></div>
    <script type="text/javascript" src="{{ asset('js/manifest.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/all.js') }}"></script>
</body>
</html>