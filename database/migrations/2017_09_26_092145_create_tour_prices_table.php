<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(false);
            $table->integer('tour_id')->unsigned();
            $table->integer('tour_variation_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->string('description', 250)->nullable();
            $table->enum('type', ['regular', 'private'])->default('regular');
            
            $table->tinyInteger('unit_min_pax')->default(0);
            $table->tinyInteger('unit_max_pax')->default(0);

            $table->tinyInteger('min_units')->default(0);
            $table->tinyInteger('max_units')->default(0);
            $table->tinyInteger('min_adults')->default(0);
            $table->tinyInteger('max_adults')->default(0);
            $table->tinyInteger('min_childs')->default(0);
            $table->tinyInteger('max_childs')->default(0);
            $table->tinyInteger('min_infants')->default(0);
            $table->tinyInteger('max_infants')->default(0);

            $table->tinyInteger('chd_min_age')->unsigned()->default(0);
            $table->tinyInteger('chd_max_age')->unsigned()->default(0);
            $table->tinyInteger('inf_min_age')->unsigned()->default(0);
            $table->tinyInteger('inf_max_age')->unsigned()->default(0);
            $table->tinyInteger('min_age')->unsigned()->default(0);
            $table->tinyInteger('max_age')->unsigned()->default(99);
            $table->boolean('strict_ages')->default(false)
                  ->comment('If true, guide or other seller will be prompted to enter dates of birth for every passenger');

            $table->decimal('unit_price', 8, 2)->default(0.00);
            $table->decimal('adult_price', 8, 2)->default(0.00);
            $table->decimal('child_price', 8, 2)->default(0.00);
            $table->decimal('infant_price', 8, 2)->default(0.00);
            $table->char('currency_id', 3);

            $table->timestampTz('starts_at')->nullable();
            $table->timestampTz('ends_at')->nullable();
            
            $table->boolean('promotion')->default(false);
            $table->timestampTz('promotion_starts_at')->nullable();
            $table->timestampTz('promotion_ends_at')->nullable();
            $table->timestampsTz();

            $table->index(['active', 'tour_id', 'tour_variation_id', 'location_id', 'company_id'], 'active_tour_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_prices');
    }
}
