<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(true);
            $table->integer('tour_id')->unsigned();
            $table->integer('tour_variation_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->integer('company_id')->unsigned();

            $table->timestampTz('starts_at')->nullable();
            $table->timestampTz('ends_at')->nullable();

            $table->smallInteger('quota')->unsigned()->default(0)->comment('default quota for tour variation');
            $table->boolean('mon')->default(false);
            $table->boolean('tue')->default(false);
            $table->boolean('wed')->default(false);
            $table->boolean('thu')->default(false);
            $table->boolean('fri')->default(false);
            $table->boolean('sat')->default(false);
            $table->boolean('sun')->default(false);

            $table->boolean('daily')->storedAs('(CASE WHEN (mon+tue+wed+thu+fri+sat+sun) < 7 THEN 0 ELSE 1 END)');
            
            $table->timestampsTz();

            $table->index(['active', 'tour_id', 'tour_variation_id', 'location_id', 'company_id'], 'active_tour_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_dates');
    }
}
