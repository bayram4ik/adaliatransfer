<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Companies
        DB::table('companies')->insert([
            'name' => 'Akay Travel',
            'type' => 'company',
            'code' => 'AKY',
            'own' => true,
            'active' => true,
            'details' => json_encode(['address' => 'Akay Plaza, Aspendos Bulvarı No: 214 Antalya / Turkey',
        							  'phone' => '+90 242 312 00 01',
            						  'fax'   => '+90 242 321 88 41, +90 242 321 88 42',
            						  'email' => 'info@akaytour.com, travel@akaytour.com'])
        ]);
        DB::table('companies')->insert([
            'name' => 'FIT Holidays',
            'type' => 'company',
            'code' => 'FIT',
            'own' => true,
            'active' => true,
            'details' => json_encode(['address' => 'Akay Plaza, Aspendos Bulvarı No: 214/C Antalya / Turkey', 
            						  'phone' => '+90 242 321 99 00',
            						  'fax'   => '+90 242 321 29 29',
            						  'email' => 'info@fitholidays.com'])
        ]);
        // Operators
        DB::table('companies')->insert([
        	'name' => 'Novaturas Latvia',
        	'type' => 'operator',
            'code' => 'NL',
        	'own'  => false,
        	'active' => true,
        	'parent_id' => 1
        ]);

        DB::table('companies')->insert([
        	'name' => 'Biblio Globus',
        	'type' => 'operator',
            'code' => 'TAE',
        	'own'  => false,
        	'active' => true,
        	'parent_id' => 2
        ]);

        // Suppliers
        DB::table('companies')->insert([
        	'name' => 'Tripology Tourism',
        	'type' => 'supplier',
        	'own'  => false,
        	'active' => true,
        	'details' => json_encode(['email' => 'opr@tripology.com.tr'])
        ]);

        DB::table('companies')->insert([
        	'name' => 'Olympos Teleferik',
        	'type' => 'supplier',
        	'own'  => false,
        	'active' => true,
        	'details' => json_encode(['email' => 'info@olymposteleferik.com'])
        ]);

        // Agencies
        DB::table('companies')->insert([
        	'name' => 'Zem Expert',
        	'type' => 'agency',
        	'own'  => false,
        	'active' => true,
        	'parent_id' => 2,
        	'details' => json_encode(['email' => 'info@zemexpert.com', 'address' => 'Istanbul'])
        ]);
    }
}
