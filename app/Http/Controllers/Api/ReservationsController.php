<?php namespace App\Http\Controllers\Api;

use App\Reservation;
use App\Http\Resources\Reservation as ReservationResource;
use Illuminate\Http\Request;

class ReservationsController extends Controller
{
    private $rules = [
        'type'                  => '',
        'agency_id'             => '',
        'pickup_location_id'    => '',
        'pickup_address'        => '',
        'dropoff_location_id'   => '',
        'dropoff_address'       => '',
        'pickup_at'             => '',
        'flight'                => '',
        'flight_at'             => '',
        'adults'                => '',
        'children'              => '',
        'baggages'              => '',
        'child_seats'           => '',
        'total'                 => '',
        'discount'              => '',
        'currency'              => '',
        'prepaid'               => '',
        'paid'                  => '',
        'agency_comission'      => '',
        'payment_currency'      => '',
        'status'                => '',
        'client_id'             => '',
        'client_name'           => '',
        'client_phone'          => '',
        'client_email'          => '',
        'client_comment'        => '',
        'vehicle_id'            => '',
        'plate'                 => '',
        'duration'              => '',
        'distance'              => '',
        'return_reservation_id' => '',
        'comission_receipt_id'  => '',
        'confirmed_at'          => '',
        'accepted_at'           => '',
        'rejected_at'           => '',
        'canceled_at'           => ''
    ];

    public function index(Request $request)
    {
        $reservations = Reservation::with([
            'agency',
            'pickup_location',
            'pickup_location.parent',
            'dropoff_location',
            'dropoff_location.parent',
            'client',
            'vehicle',
            //'return_reservation',
            'comission_receipt'
        ])
        ->whereBetween('pickup_at', [$request->input('start_date'), $request->input('end_date')])
        ->get();
        return ReservationResource::collection($reservations);
    }

    public function show(Reservation $reservation)
    {
        ReservationResource::withoutWrapping();
        return new ReservationResource($reservation);
    }

    public function store(Request $request)
    {
        $reservation = $request->validate($this->rules);
        Reservation::create($reservation);
    }

    public function update(Request $request, Reservation $reservation)
    {

    }

    public function destroy(Reservation $reservation)
    {
        if($reservation->delete()) {
            return ['success' => true];
        }
    }

    public function restore($id)
    {
        $reservation = Reservation::findOrFail($id);
        $reservation->restore();
    }
}
