<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model{
    use SoftDeletes;

    protected $casts = [
        'services' => 'array',
    ];

    protected $dates = ['deleted_at'];

    public function agency(){
        return $this->belongsTo(User::class);
    }

    public function pickup_location(){
        return $this->belongsTo(Location::class);
    }

    public function dropoff_location(){
        return $this->belongsTo(Location::class);
    }

    public function client(){
        return $this->belongsTo(User::class);
    }

    public function vehicle(){
        return $this->belongsTo(Vehicle::class);
    }

    public function comission_receipt(){
        return $this->belongsTo(ComissionReceipt::class);
    }
}
