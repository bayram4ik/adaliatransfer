require('./bootstrap')

import Vue from 'vue'
import router from './router'
import store from './store'
import App from './components/App.vue'
//import { sync } from 'vuex-router-sync'
import collect from 'collect.js'
import moment from 'moment'
//import VueI18n from 'vue-i18n'

import {
  //Pagination,
  Dialog,
  //Autocomplete,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  //Menu,
  //Submenu,
  //MenuItem,
  //MenuItemGroup,
  Input,
  InputNumber,
  Radio,
  RadioGroup,
  RadioButton,
  Checkbox,
  CheckboxButton,
  CheckboxGroup,
  Switch,
  Select,
  Option,
  OptionGroup,
  Button,
  ButtonGroup,
  Table,
  TableColumn,
  DatePicker,
  TimeSelect,
  TimePicker,
  Popover,
  Tooltip,
  //Breadcrumb,
  //BreadcrumbItem,
  Form,
  FormItem,
  Tabs,
  TabPane,
  Tag,
  Tree,
  Alert,
  //Slider,
  //Icon,
  Row,
  Col,
  Upload,
  //Progress,
  //Badge,
  //Card,
  //Rate,
  //Steps,
  //Step,
  //Carousel,
  //CarouselItem,
  //Collapse,
  //CollapseItem,
  Cascader,
  //ColorPicker,
  Transfer,
  //Container,
  //Header,
  //Aside,
  //Main,
  //Footer,
  Loading,
  MessageBox,
  Message,
  Notification
} from 'element-ui'

import 'element-ui/lib/theme-chalk/index.css'

import lang from 'element-ui/lib/locale/lang/tr-TR'
import locale from 'element-ui/lib/locale'
locale.use(lang)

Vue.prototype.$ELEMENT = { size: 'small' }

//Vue.use(Pagination)
Vue.use(Dialog)
//Vue.use(Autocomplete)
Vue.use(Dropdown)
Vue.use(DropdownMenu)
Vue.use(DropdownItem)
//Vue.use(Menu)
//Vue.use(Submenu)
//Vue.use(MenuItem)
//Vue.use(MenuItemGroup)
Vue.use(Input)
Vue.use(InputNumber)
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(RadioButton)
Vue.use(Checkbox)
Vue.use(CheckboxGroup)
Vue.use(Switch)
Vue.use(Select)
Vue.use(Option)
Vue.use(OptionGroup)
Vue.use(Button)
Vue.use(ButtonGroup)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(DatePicker)
Vue.use(TimeSelect)
Vue.use(TimePicker)
Vue.use(Popover)
Vue.use(Tooltip)
//Vue.use(Breadcrumb)
//Vue.use(BreadcrumbItem)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Tag)
Vue.use(Tree)
Vue.use(Alert)
//Vue.use(Slider)
//Vue.use(Icon)
Vue.use(Row)
Vue.use(Col)
Vue.use(Upload)
//Vue.use(Progress)
//Vue.use(Badge)
//Vue.use(Card)
//Vue.use(Rate)
//Vue.use(Steps)
//Vue.use(Step)
//Vue.use(Carousel)
//Vue.use(CarouselItem)
//Vue.use(Collapse)
//Vue.use(CollapseItem)
Vue.use(Cascader)
//Vue.use(ColorPicker)
//Vue.use(Container)
//Vue.use(Header)
//Vue.use(Aside)
//Vue.use(Main)
//Vue.use(Footer)

Vue.use(Loading.directive)

Vue.prototype.$loading = Loading.service
Vue.prototype.$msgbox = MessageBox
Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$prompt = MessageBox.prompt
Vue.prototype.$notify = Notification
Vue.prototype.$message = Message

//Vue.use(VueI18n)

moment.locale('tr')
//sync(store, router)

/*Vue.component('multi-select', {
	template: `
		<select multiple @input="update" @change="update">
			<option v-for="record in activeOptions"
					:value="record.id"
					:selected="selectedOptions.includes(record.id)">{{ record.name }}</option>
		</select>
	`,
	props: {
		options: { type: Object },
		value: { type: Array },
		buttonWidth: { type: String, default: '100%' },
		filtering: { type: Boolean, default: true },
		ciFiltering: { type: Boolean, default: true },
		selectAllOption: { type: Boolean, default: true },
		clickableOptGroups: { type: Boolean, default: true },
		filterBehavior: { type: String, default: 'both' }
	},
	data(){
		return {
			chosen: this.value,
			items: this.options
		}
	},
	mounted(){
		this.initPlugin(this)
	},
	methods: {
		update(){
			this.$emit('update', this.chosen)
		},
		initPlugin(vm){
			this.$nextTick(function () {
				$(vm.$el).multiselect({
					templates: {
					    filterClearBtn: '',
					    li: '<li><a href="javascript:void(0);"><label></label></a></li>',
					},
			        buttonWidth: vm.buttonWidth,
			        enableFiltering: vm.filtering,
			        enableCaseInsensitiveFiltering: vm.ciFiltering,
			        includeSelectAllOption: vm.selectAllOption,
			        enableClickableOptGroups: vm.clickableOptGroups,
			        filterBehavior: vm.filterBehavior,
			        onChange: function(option, checked, select) {
			            $.uniform.update()
			            if(checked && !vm.selectedOptions.includes(option[0].value)){
			            	vm.chosen.push(parseInt(option[0].value))
			            }else{
			            	vm.chosen.splice(parseInt(option[0].index), 1)
			            }
			            vm.update()
			        },
			        onSelectAll: function(selectedAll) {
			        	$.uniform.update()
			        	vm.chosen.splice(0)
			        	if(selectedAll == true){
				        	vm.chosen.splice(0, 1, ...collect(vm.activeOptions).keys().all().map(i => parseInt(i)))
				    	}
				    	vm.update()
			        }
			    })
				$(".multiselect-container input").uniform({
				    radioClass: 'choice'
				})
      		})
		}
	},
	computed: {
		selectedOptions: function(){
			return this.chosen
		},
		activeOptions: function(){
			return this.items
		}
	},
	watch: {
		options: {
			handler: function(val, old){
				this.items = val
				$(this.$el).multiselect('destroy')
				this.initPlugin(this)
			},
			deep: true
		}
	}
})*/

Vue.component('switchery', {
	template: `<input type="checkbox" :checked="value" @input="update()" @click="update()" />`,
	props: {
		value: { type: Boolean },
		color: { type: String, default: '#64bd63' },
		secondaryColor: { type: String, default: '#dfdfdf' },
		jackColor: { type: String, default: '#fff' },
		jackSecondaryColor: { type: String, default: null },
		className: { type: String, default: 'switchery' },
		disabled: { type: Boolean, default: false },
		disabledOpacity: { type: Number, default: 0.5 },
		speed: { type: String, default: '0.5s' },
		size: { type: String, default: 'small' },
		type: { type: String, default: 'success' }
	},
	mounted: function(){
		let typeColor = this.color
		switch(this.type){
			case 'success':
				typeColor = '#64bd63'
				break
			case 'primary':
				typeColor = '#2196F3'
				break
			case 'danger':
				typeColor = '#EF5350'
				break
			case 'warning':
				typeColor = '#FF7043'
				break
			case 'info':
				typeColor = '#00BCD4'
				break
		}
		let options = {
			color: typeColor,
			secondaryColor: this.secondaryColor,
			jackColor: this.jackColor,
			jackSecondaryColor: this.jackSecondaryColor,
			className: this.className,
			disabled: this.disabled,
			disabledOpacity: this.disabledOpacity,
			speed: this.speed,
			size: this.size
		}

		var switchery = new Switchery(this.$el, options)
	},
	methods: {
		update(){
			this.$emit('input', this.value)
		}
	}
})

/*Vue.component('date-picker', {
	//template: '<button type="button" class="btn btn-link text-semibold"><i class="icon-calendar3 position-left"></i> <span>{{ dateRange }}</span> <b class="caret"></b></button>',
	template: '<span class="daterange-custom-display" v-html="rangeFormatted"></span>',
	props: {
  		startDate: {
  			type: Object,
  			default: () => moment().startOf('day')
  		},
  		endDate: {
  			type: Object,
  			default: () => moment().endOf('day')
  		},
  		singleDatePicker: {
  			type: Boolean,
  			default: false
  		},
  		timePicker: {
  			type: Boolean,
  			default: true
  		},
  		minDate: {
  			type: String,
  			default: null
  		},
		maxDate: {
  			type: String,
  			default: null
		},
		dateLimit: {
			type: Number,
			default: 366
		},
		opens: {
			type: String,
			default: 'left'
		},
		autoApply: {
			type: Boolean,
			default: false
		},
		linkedCalendars: {
			type: Boolean,
			default: false
		},
		applyClass: {
			type: String,
			default: 'btn-small bg-success-600 btn-block'
		},
		cancelClass: {
			type: String,
			default: 'btn-small btn-default btn-block'
		},
		format: {
			type: String,
			default: 'MMM D HH:mm'
		},
		ranges: {
			type: Object,
			default: () => {
				return {
	                'Bugun': [moment().startOf('day'), moment().endOf('day')],
	                'Yarin': [moment().add(1, 'days').startOf('day'), moment().add(1, 'days').endOf('day')],
	                'Dun': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
	                'Son 7 gun': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
	                'Son 30 gun': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
	                'Bu ay': [moment().startOf('month').startOf('day'), moment().endOf('month').endOf('day')],
	                'Onceki ay': [moment().subtract(1, 'month').startOf('month').startOf('day'), moment().subtract(1, 'month').endOf('month').endOf('day')]
	            }
			}
		}
	},
	data() {
		return {
			startsAt: moment().startOf('day'),
			endsAt: moment().endOf('day')
		}
	},
	mounted: function() {
		var $vm = this;
		this.startsAt = this.startDate;
		this.endsAt = this.endDate;
	    $(this.$el).daterangepicker(
	        {
	            startDate: moment($vm.startDate),
	            endDate: moment($vm.endDate),
	            timePicker: $vm.timePicker,
	            timePicker24Hour: $vm.timePicker,
	            singleDatePicker: $vm.singleDatePicker,
	            //minDate: $vm.minDate,
	            //maxDate: $vm.maxDate,
	            dateLimit: { days: $vm.dateLimit },
	            opens: $vm.opens,
	            autoApply: $vm.autoApply,
	            linkedCalendars: $vm.linkedCalendars,
	            applyClass: $vm.applyClass,
	            cancelClass: $vm.cancelClass,
	            format: $vm.format,
	            ranges: $vm.ranges
	        },
	        function(start, end) {
	        	$vm.startsAt = start;
	        	$vm.endsAt = end;
	        	$vm.$emit('updateDateRange', { start, end });
	        }
	    );
	},
	computed: {
		dateRange: function() {
			if(this.startsAt.format(this.format) !== this.endsAt.format(this.format)){
				return this.startsAt.format(this.format) + ' - ' + this.endsAt.format(this.format);
			}else{
				return this.startsAt.format(this.format);
			}
		},
		rangeFormatted(){
			return '<i>' + this.startsAt.format('D') + '</i><b><i>' + this.startsAt.format('MMM') + '</i> <i>' + this.startsAt.format('YYYY') + '</i></b> <em>&#8211;</em> <i>' + this.endsAt.format('D') + '</i><b><i>' + this.endsAt.format('MMM') + '</i> <i>' + this.endsAt.format('YYYY') + '</i></b><i class="icon-calendar3 position-left" style="font-size: 20px;margin: 0 0 12px 12px;"></i>';
		}
	}
})*/

Vue.filter('moment', function(val, format){
	if(val){
		return moment(String(val)).format(format)
	}
})

Vue.filter('fromNow', function(val){
	if(val){
		return moment(String(val)).fromNow()
	}
})

/*Vue.directive('uniform', {
	inserted: function (el) {
		$(el).uniform({
			radioClass: 'choise'
		})
	}
})*/

new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App)
})