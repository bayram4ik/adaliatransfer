<!DOCTYPE html>
<html lang="{{ session('locale') }}">
<head>
	@if(!isset($indexing) || $indexing === true)
		<meta name="robots"
			content="{{ $settings->indexing ? 'index, follow' : 'noindex, nofollow' }}">
	@else
		<meta name="robots" content="noindex, nofollow">
	@endif
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="@yield('metadescr')">
	<meta name="keywords" content="">

	<meta name='wmail-verification' content='18c548ea9bb518a251b73a35dbec85ee' />

	<meta property="og:description" content="{{ $settings->meta_description }}">
	<meta property="og:image" content="{{ asset('assets/images/adaliatransferlogo.png') }}">
	<meta property="og:site_name" content="">
	<meta property="og:title" content="Adalia Transfer">
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://adaliatransfer.com">
	<link rel="icon" type="image/png" href="{{ asset('assets/images/adaliatransferlogo.png') }}">
	<title>@yield('title') - {{ $settings->site_name }}</title>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="{{ asset('assets/css/swiper.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/css/swipebox.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/css/zoomslider.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/css/style.css?03') }}" rel="stylesheet">
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:700,800%7COpen+Sans:400,600,700" rel="stylesheet">
	<script type="text/javascript" src="{{ asset('assets/js/modernizr-2.6.2.min.js') }}"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.min.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.bootstrap3.min.css" rel="stylesheet">
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter47524945 = new Ya.Metrika2({ id:47524945, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/tag.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks2");</script>
	<!-- /Yandex.Metrika counter -->
	<!-- BEGIN JIVOSITE CODE {literal} -->
	<script type='text/javascript'>
	(function(){ var widget_id = 'LrTWJzog9N';var d=document;var w=window;function l(){
	  var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
	  s.src = '//code.jivosite.com/script/widget/'+widget_id
	    ; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}
	  if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}
	  else{w.addEventListener('load',l,false);}}})();
	</script>
	<!-- {/literal} END JIVOSITE CODE -->

	<script>
		window.currencies = {!! json_encode($currencies) !!}
		window.rates = {!! json_encode($rates) !!}
		window.settings = {!! json_encode($settings) !!}
	</script>
	<style>
		.main-search * {
			text-align: left !important;
		}
		.comments-area .comment-info:after{
			background: none;
		}
		.comments-area .comment-info{
			padding-bottom: 32px;
		}
		.page-header{
			margin-bottom: 25px !important;
		}
		.form-control.selectize-control{
			height: 48px !important;
			max-height: 48px !important;
		}
		.selectize-input{
			max-height: 48px !important;
		}
		.selectize-dropdown, .selectize-input, .selectize-input input{
			line-height: 34px !important;
		}
		.selected, .selected:hover{
			background-color: #656565 !important;
		}
		.other-locations a{
			color: blue;
		}
		.accordion h4{
			outline-color: transparent !important;
		}
		@media (min-width: 991px){
			.partner {
		    	min-height: 500px !important;
			}
		}
		.pt-0{
			padding-top:0px !important;
		}
	</style>
	@stack('styles')
</head>

<body>
	<div id="app">
	<div class="navbar-dark-transparent">
		<div class="nav-wrapper" id="nav-wrapper">
			<nav class="navbar navbar-static navbar-affix" data-spy="affix">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar top-bar"></span>
							<span class="icon-bar middle-bar"></span>
							<span class="icon-bar bottom-bar"></span>
						</button>
						<a class="logo" href="/">
							<img src="{{ asset('assets/images/adaliatransferlogo.png') }}" alt="Adalia Transfer" style="height: 82px;">
						</a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<button type="button" class="navbar-toggle collapsed">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar top-bar"></span>
							<span class="icon-bar middle-bar"></span>
							<span class="icon-bar bottom-bar"></span>
						</button>
						<ul class="nav navbar-nav">
							<li class="current_page_item">
								<a href="/"><i class="fa fa-home"></i></a>
							</li>

							<li class="hasSub">
								<a href="{{ route('services') }}">{{ trans('frontend.services') }}</a>
								@php
									$services = App\Service::select('name', 'slug')->get();
								@endphp
								<ul class="sub-menu">
									@foreach($services as $service)
									<li>
										<a href="{{ route('services', ['type' => $service->slug]) }}">
											{{ $service->name }}</a>
									</li>
									@endforeach
								</ul>
							</li>
							<li>
								<a href="{{ route('howitworks') }}">
									{{ trans('frontend.how-it-works') }}
								</a>
							</li>
							<li>
								<a href="{{ route('contacts') }}">{{ trans('frontend.contacts') }}</a>
							</li>
							<li>
								<a href="{{ route('login') }}">{{ trans('frontend.login') }}</a>
							</li>
							<li class="hasSub">
								<a href="#">
								<img id="currency__img" class="lang__img" width="16" height="11" src="{{ asset('images/flags/' . session('locale') . '.png') }}" alt="{{ session('locale') }}">
								@{{ activeCurrency }}</a>
								<ul class="sub-menu" style="color: #000;width:320px;padding: 10px 20px;">
									<small>{{ trans('frontend.currency') }}:</small>

									<a v-for="currency in currencies"
									   href="javascript:;"
									   style="padding:24px 5px;"
									   @click.prevent="changeCurrency(currency.id)"
									   v-text="currency.id">
									</a>
									<hr style="margin:5px;">
									<small>{{ trans('frontend.language') }}:</small>
									@foreach($settings->active_languages as $lang)
										@if(session('locale') == $lang)
											<span class="label label-default">
												{{ $languages[$lang]->name }}
											</span>
										@else

											<a href="{{ LaravelLocalization::getLocalizedURL($lang) }}" style="padding:24px 5px;text-transform:none">
												{{ $languages[$lang]->name }}
											</a>
										@endif
									@endforeach
								</ul>
							</li>

						</ul>

					</div>
				</div>
			</nav>
		</div>
	</div>

	@yield('content')

	<section id="block-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 col-ms-6">
					<h4>{{ trans('frontend.about-us') }}</h4>
					<p>{{ $settings->about_us_short }}</p>
					<div class="social-small social-yellow">
						<a href="{{ $settings->social->fb }}" class="fa fa-facebook"></a>
						<a href="#" class="fa fa-instagram"></a>
						<a href="#" class="fa fa-google-plus"></a>
					</div>
				</div>
				<div class="col-lg-5 col-md-5 hidden-md hidden-sm hidden-xs hidden-ms">
					<h4>{{ trans('frontend.explore') }}</h4>
					<div class="row">
						<div class="col-md-5">
							<ul class="nav navbar-nav">
								<li class="active">
									<a href="#">{{ trans('frontend.book-transfer') }}</a>
								</li>
								<li>
									<a href="{{ route('services') }}">{{ trans('frontend.services') }}</a>
								</li>
								<li>
									<a href="{{ route('faq') }}">{{ trans('frontend.faq') }}</a>
								</li>
								<li>
									<a href="{{ route('partner') }}">{{ trans('frontend.become-partner') }}</a>
								</li>
							</ul>
						</div>
						<div class="col-md-5">
							<ul class="nav navbar-nav">
								<li>
									<a href="{{ route('reviews') }}">{{ trans('frontend.reviews') }}</a>
								</li>
								<li>
									<a href="{{ route('howitworks') }}">{{ trans('frontend.how-it-works') }}</a>
								</li>
								<li>
									<a href="{{ route('contacts') }}">{{ trans('frontend.contacts') }}</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-ms-6">
					<h4>{{ trans('frontend.contact-us') }}</h4>
					<p>
						<span class="yellow">{{ trans('frontend.address') }}:</span> {!! $settings->company_address !!}
					</p>
					<ul class="address">
						<li>
							<span class="fa fa-phone"></span>
							<a href="tel://{{ $settings->company_phone }}">
							{{ $settings->company_phone }}
							</a>
						</li>
						<li>
							<span class="fa fa-envelope"></span>
							<a href="mailto:{{ $settings->company_email }}">{{ $settings->company_email }}</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<div class="container">
			<a href="{{ URL::to('/') }}">
				{{ $settings->site_name }}
			</a>
			&bull;
			{{ $settings->company_foundation_year }}-{{ date('Y') }} © {{ trans('frontend.rights') }} &bull;
			<a href="{{ route('terms') }}">{{ trans('frontend.terms') }}</a>
			<a href="#" class="go-top hidden-xs hidden-ms"></a>
		</div>
	</footer>
	</div>
	<script>var base_href = '/';</script>
	<script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="{{ asset('assets/js/plugins.min.js') }}"></script>
	<script src="{{ asset('assets/js/map-style.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCTRSHf8sjMCfK9PHPJxjJkwrCIo5asIzE"></script>
	<script type="text/javascript" src="{{ asset('assets/js/scripts.js') }}"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
	<script type="text/javascript">
		Vue.directive('selectize', {
			inserted: function (el) {
				$(el).selectize({
					sorField: 'text',
					searchField: ['text', 'code', 'trans', 'id'],
					lockOptgroupOrder: true,
				});
			}
		});

		var currencyMixin = {
			data: function(){
				return {
					activeCurrency: 'EUR',
					currencies: {!! json_encode($currencies) !!},
					rates: {!! json_encode($rates) !!}
				}
			},
			created: function(){
				var curr = localStorage.getItem('activeCurrency');
				this.activeCurrency = curr || 'EUR'
			},
		 	methods: {
				convert: function(amount, from, insertCurr){
					if(!isNaN(amount) && (function(x) { return (x | 0) === x; })(parseFloat(amount))){
						if(insertCurr !== false){
							var insertCurr = true;
						}
						var total = 0;
						if(this.activeCurrency !== from){
							var total = parseFloat(amount)*parseFloat(this.rates[from].selling);
							if(this.activeCurrency !== 'TRY'){
								total = total/parseFloat(this.rates[this.activeCurrency].selling);
							}
						}else{
							total = amount
						}
						if(insertCurr){
							return this.formatCurrency(total, this.activeCurrency)
						}else{
							return Math.ceil(total)
						}
					}
				},
				formatCurrency: function(amount, currency){
					return Math.ceil(amount) + ' ' + this.currencies[currency].symbol
				},
				changeCurrency: function(curr){
					localStorage.setItem('activeCurrency', curr);
					this.activeCurrency = curr;
				}
		  	}
		};

	</script>
	@stack('scripts')
</body>
</html>