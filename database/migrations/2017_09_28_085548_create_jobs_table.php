<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->integer('company_id')->unsigned();
            
            $table->integer('duty_id')->unsigned();
            $table->enum('type', ['arrival', 'departure', 'tour',  'sub', 'ferry'])
                  ->comment('ferry is for empty job or blank return; sub is for transfers from one to another region');
            $table->enum('subtype', ['collect', 'uncollect'])->nullable();

            $table->enum('service_type', ['regular', 'shuttle', 'private',])->default('regular');
            $table->enum('vehicle_type', ['mini', 'midi', 'bus']);
            $table->char('vehicle_id', 10)->nullable();
            $table->char('group', 10)->nullable();
            $table->tinyInteger('capacity')->default(0);

            $table->integer('driver_id')->unsigned()->nullable();
            $table->integer('spare_driver_id')->unsigned()->nullable();


            $table->integer('guide_id')->unsigned()->nullable();
            $table->integer('meeting_point_id')->unsigned()->nullable();
            $table->timestampTz('meeting_time')->nullable();

            $table->integer('tourguide_id')->unsigned()->nullable();
            $table->integer('tourguide_meeting_point_id')->unsigned()->nullable();
            $table->timestampTz('tourguide_meeting_time')->nullable();

            $table->boolean('driver_notified')->default(0);
            $table->boolean('guide_notified')->default(0);
            $table->boolean('tourguide_notified')->default(0);

            $table->tinyInteger('adults')->default(0);
            $table->tinyInteger('childs')->default(0);
            $table->tinyInteger('infants')->default(0);

            $table->tinyInteger('total_pax')->storedAs('adults+childs+infants');

            $table->decimal('start_lat', 11, 8)->default(0);
            $table->decimal('start_lon', 11, 8)->default(0);
            $table->decimal('end_lat', 11, 8)->default(0);
            $table->decimal('end_lon', 11, 8)->default(0);
            $table->decimal('km', 6, 2)->default(0);
            $table->smallInteger('mins')->default(0);

            $table->datetimeTz('job_ends_at')->storedAs('CASE WHEN COALESCE(meeting_time, tourguide_meeting_time) IS NULL THEN NULL ELSE DATE_ADD(COALESCE(meeting_time, tourguide_meeting_time), INTERVAL mins MINUTE) END')->nullable();

            $table->boolean('transport_overloaded')
                  ->storedAs('CASE WHEN capacity < total_pax+(CASE WHEN guide_id IS NOT NULL THEN 1 ELSE 0 END)+(CASE WHEN tourguide_id IS NOT NULL THEN 1 ELSE 0 END) THEN 1 ELSE 0 END');
            $table->string('notes', 2000)->nullable();
            $table->integer('created_user_id')->unsigned()->nullable();
            $table->integer('updated_user_id')->unsigned()->nullable();
            $table->integer('deleted_user_id')->unsigned()->nullable();
            $table->timestampsTz();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_jobs');
    }
}
