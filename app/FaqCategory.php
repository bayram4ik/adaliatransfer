<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
class FaqCategory extends Model{
    use HasTranslations;
    public $translatable = ['name'];
    public $guarded = [];
    public $timestamps = false;

    public function faqs(){
        return $this->hasMany(Faq::class, 'category_id');
    }
}