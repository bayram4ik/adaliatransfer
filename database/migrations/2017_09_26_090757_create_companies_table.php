<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Kalnoy\Nestedset\NestedSet;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->char('code', 20)->nullable();
            $table->enum('type', ['company', 'operator', 'agency', 'supplier']);
            $table->boolean('own', 0);
            $table->boolean('active', 0);
            $table->jsonb('details')->nullable()->comment('Address details, branch offices etc.');
            $table->integer('location_id')->nullable();
            NestedSet::columns($table);
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
