<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\User;
use App\Setting;
use Auth;

class BackendController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
        $user = User::with('roles:name', 'permissions:name')->findOrFail(Auth::id());
        $roles = json_encode($user->roles->pluck('name'));
        $permissions = json_encode($user->permissions->pluck('name'));
        $settings = Setting::with('attachments')->first();
        return view('backend', compact('user', 'roles', 'permissions', 'settings'));
    }
}
