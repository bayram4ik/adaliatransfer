<?php namespace App\Http\Middleware;

use Closure;
use App\Language;
use App\Currency;

class SetPreferedLanguage{
    public function handle($request, Closure $next){
        if(!session('currency')){
            session(['currency' => Currency::where('active', true)->where('main', true)->first()->id]);
        }
        return $next($request);
    }
}
