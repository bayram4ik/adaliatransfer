<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model{
	
    public $incrementing = false;
    public function rates(){
        return $this->hasMany(ExchangeRate::class);
    }
}
