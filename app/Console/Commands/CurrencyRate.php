<?php namespace App\Console\Commands;

use App\ExchangeRate;
use App\Currency;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Console\Command;

class CurrencyRate extends Command
{
    protected $signature = 'currency:rates {date?} {endDate?}';
    protected $description = 'Get TCMB rates';

    public function handle()
    {
        $begin = new DateTime($this->argument('date') ?? date('Y-m-d', strtotime('yesterday')));
        $end   = $this->argument('endDate') ? new DateTime($this->argument('endDate')) : null;

        if ($end && $begin != $end) {
            $period  = new DatePeriod($begin, DateInterval::createFromDateString('1 day'), $end);
            foreach ($period as $dt) {
                $this->getRates($dt);
            }
        } else {
            $this->getRates($begin);
        }
    }

    private function getRates($dt)
    {
        $rates = [];

        @$openmbxml = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/' . $dt->format('Ym') . '/' . $dt->format('dmY') . '.xml');
        if (! $openmbxml) {
            $this->error($dt->format('Y-m-d') . ' - retrying in 1 sec');
            sleep(1);
            if (! $openmbxml) {
                $this->error($dt->format('Y-m-d') . ' - retrying in 3 sec');
                sleep(3);
                @$openmbxml = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/' . $dt->format('Ym') . '/' . $dt->format('dmY') . '.xml');
                if (! $openmbxml) {
                    $this->error($dt->format('Y-m-d') . ' - retrying in 5 sec');
                    sleep(5);
                    @$openmbxml = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/' . $dt->format('Ym') . '/' . $dt->format('dmY') . '.xml');
                }
            }
        }

        if (! $openmbxml) {
            $this->error($dt->format('Y-m-d') . ' - FAILED');
        } else {
            @$rates['USD']['buy']  = (float)$openmbxml->Currency[0]->ForexBuying;
            @$rates['EUR']['buy']  = (float)$openmbxml->Currency[3]->ForexBuying;
            @$rates['GBP']['buy']  = (float)$openmbxml->Currency[4]->ForexBuying;
            @$rates['RUB']['buy']  = (float)$openmbxml->Currency[14]->ForexBuying;
            @$rates['USD']['sell'] = (float)$openmbxml->Currency[0]->ForexSelling;
            @$rates['EUR']['sell'] = (float)$openmbxml->Currency[3]->ForexSelling;
            @$rates['GBP']['sell'] = (float)$openmbxml->Currency[4]->ForexSelling;
            @$rates['RUB']['sell'] = (float)$openmbxml->Currency[14]->ForexSelling;


            $currencies = Currency::get();
            $main = $currencies->where('main', 1)->first()->id;

            foreach($currencies->where('main', 0) as $currency)
            {
                ExchangeRate::updateOrCreate(
                    ['currency_id' => $currency->id, 'date' => $dt->format('Y-m-d')], 
                    ['buying' => $rates[$currency->id]['buy'], 'selling' => $rates[$currency->id]['sell']+$currency->selling_margin]
                );
                $this->info($currency->id . ': ' . $rates[$currency->id]['buy'] . ' - ' . $rates[$currency->id]['sell']);
            }
        }

        $openmbxml = null;
    }
}
