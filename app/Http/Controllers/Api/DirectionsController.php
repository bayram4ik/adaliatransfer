<?php namespace App\Http\Controllers\Api;

use App\Direction;
use Illuminate\Http\Request;

class DirectionsController extends Controller
{
    public function index(Request $request)
    {
        return Direction::when($request->has('start_location_id'), function($query) use ($request) {
                            return $query->where('start_location_id', $request->input('start_location_id'));
                        })
                        ->get();
    }
    public function store(Request $request)
    {
        return Direction::updateOrCreate(
            ['start_location_id' => $request->input('start_location_id'), 'end_location_id' => $request->input('end_location_id')],
            ['prices' => $request->input('prices')]
        );
    }
    public function show(Direction $direction)
    {
        //
    }
    public function update(Request $request, Direction $direction)
    {
        //
    }
    public function destroy(Direction $direction)
    {
        //
    }
}
