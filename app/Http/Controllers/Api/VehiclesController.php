<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Vehicle;
use App\Events\EntryCreated;
use App\Events\EntryUpdated;
use App\Http\Resources\VehicleResource;

class VehiclesController extends Controller{

    protected $rules = [
        'pax_capacity'      => 'required',
        'baggage_capacity'  => 'required',
        'models'            => ''
    ];

    public function index()
    {
        return VehicleResource::collection(Vehicle::with('attachments')->orderBy('pax_capacity')->get());
    }

    public function store(Request $request)
    {
        $model = Vehicle::create($request->validate($this->rules));
        $model->setTranslations('name', $request->input('name_trans'));
        $model->setTranslations('models', $request->input('models_trans'));
        $model->save();
        $vehicle = new VehicleResource(Vehicle::with('attachments')->findOrFail($model->id));
        broadcast(new EntryCreated('vehicles', $vehicle->resolve()));
        return $vehicle;
    }

    public function show($id)
    {
    }

    public function update(Request $request, $id)
    {
        $model = Vehicle::find($id);
        $model->update($request->validate($this->rules));
        $model->setTranslations('name', $request->input('name_trans'));
        $model->setTranslations('models', $request->input('models_trans'));
        $model->save();
        $vehicle = new VehicleResource(Vehicle::with('attachments')->findOrFail($id));
        broadcast(new EntryUpdated('vehicles', $vehicle->resolve()));
        return $vehicle;
    }

    public function destroy($id)
    {
    
    }

    public function upload(Request $request){
        $vehicle = Vehicle::find($request->input('vehicle_id'));
        $vehicle->attach($request->file('file'));
        $vehicle = new VehicleResource(Vehicle::with('attachments')->findOrFail($request->input('vehicle_id')));
        broadcast(new EntryUpdated('vehicles', $vehicle->resolve()));
        return $vehicle;
    }

    public function deleteimage(Request $request){
        Vehicle::find($request->input('vehicle_id'))
               ->attachment($request->input('key'))
               ->delete();
        $vehicle = new VehicleResource(Vehicle::with('attachments')->findOrFail($request->input('vehicle_id')));
        broadcast(new EntryUpdated('vehicles', $vehicle->resolve()));
        return $vehicle;
    }
}
