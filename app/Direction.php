<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Direction extends Model{
    
    public $timestamps = false;
    public $guarded = [];
    public $casts = [
        'prices' => 'array'
    ];

    public function start_location(){
        return $this->belongsTo(Location::class);
    }
    public function end_location(){
        return $this->belongsTo(Location::class);
    }
}
