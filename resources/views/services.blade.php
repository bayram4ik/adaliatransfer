@extends('layouts.main')
@section('title', trans('frontend.our-services'))
@push('scripts')
<script>
    var app = new Vue({
        el: '#app',
        mixins: [currencyMixin]
    });
</script>
@endpush
@section('content')

<header class="page-header" style="background-image: url({{ asset('assets/images/_inner-bg.jpg') }});">
        <div class="container">
            <ol class="bread">
                <li>
                    <a href="{{ route('home') }}"><span><i class="fa fa-home"></i></span></a>
                </li>
                <li class="divider"><span>//</span></li>
                <li>
                    <span>{{ trans('frontend.services') }}</span>
                </li>
            </ol>
            <h1>{{ trans('frontend.our-services') }}</h1>
        </div>
    </header>

    <div class="container">
        <div class="inner row">
            <div class="col-lg-12 text-page">
                <div class="blog">
                    <div class="row">
                    @foreach($services as $service)
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="item matchHeight">
                                <a href="{{ route('services', ['type' => $service->slug]) }}" class="photo">
                                    <img src="{{ asset('images/services/' . $service->slug . '.jpg') }}" class="full-width rounded">
                                </a>
                                <div class="description">
                                    <a href="{{ route('services', ['type' => $service->slug]) }}" class="header">
                                        <h5>{{ $service->name }}</h5>
                                    </a>
                                    <p class="text">{{ $service->description }}</p>
                                    <a href="{{ route('services', ['type' => $service->slug ]) }}" class="btn btn-yellow">{{ trans('frontend.read-more') }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection