<?php namespace App\Http\Controllers\Api;

use Hash;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\AgencyResource;
use App\Events\EntryCreated;
use App\Events\EntryUpdated;

class AgenciesController extends Controller
{
    public $rules = [
        'name'              => 'required',
        'email'             => 'required|email',
        'status'            => '',
        'agency_comission'  => '',
        'agency_address'    => '',
        'agency_name'       => '',
        'phone'             => ''
    ];

    public function index()
    {
        return AgencyResource::collection(User::role('agency')->get());
    }

    public function show($id)
    {
        return new AgencyResource(User::find($id));
    }

    public function store(Request $request)
    {
        $agency = $request->validate($this->rules);
        $user = new User;
        $user->name              = $agency['name'];
        $user->email             = $agency['email'];
        $user->status            = $agency['status'];
        $user->password          = Hash::make($request->input('password'));
        $user->details = [
            'agency_comission'  => $agency['agency_comission'],
            'agency_address'    => $agency['agency_address'],
            'agency_name'       => $agency['agency_name'],
            'phone'             => $agency['phone']
        ];
        $user->save();
        $user->assignRole('agency');
        if($request->input('can_pay_partial') === true){
            $user->givePermissionTo('pay-partial');
        }
        $user = new AgencyResource(User::find($user->id));
        broadcast(new EntryCreated('agencies', $user->resolve()));
        return $user;
    }

    public function update(Request $request, $id)
    {
        $data = $request->validate($this->rules);
        $user = User::findOrFail($id);

        $user->name              = $data['name'];
        $user->email             = $data['email'];
        $user->status            = $data['status'];
        if($request->has('password')){
            $user->password     = Hash::make($request->input('password'));
        }
        $user->details = [
            'agency_comission'  => $data['agency_comission'],
            'agency_address'    => $data['agency_address'],
            'agency_name'       => $data['agency_name'],
            'phone'             => $data['phone']
        ];
        $user->save();

        if($request->input('can_pay_partial') === true){
            $user->givePermissionTo('pay-partial');
        }else{
            $user->revokePermissionTo('pay-partial');
        }
        $user = new AgencyResource(User::find($user->id));
        broadcast(new EntryUpdated('agencies', $user->resolve()));
        return $user;
    }

    public function destroy(User $user)
    {

    }
}
