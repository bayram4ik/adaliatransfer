<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Location;
use App\Events\EntryCreated;
use App\Events\EntryUpdated;
use Cache;
use App\Http\Resources\LocationResource;

class LocationsController extends Controller
{
    public $rules = [
        'name'      => 'required',
        'type'      => 'required',
        'parent_id' => ''
    ];

    public function index(Request $request)
    {
        $locations = Location::when($request->has('types'), function($query) use ($request) { 
                return $query->whereIn('type', $request->input('types'));
            })
            ->with('children')
            ->with('parent', 'parent.parent')
            ->get();
        return LocationResource::collection($locations);
    }

    public function store(Request $request)
    {
        $location = Location::create($request->validate($this->rules));
        broadcast(new EntryCreated('locations', (new LocationResource($location))->resolve()));
        return $location;
    }

    public function update(Request $request, $id)
    {
        $location = Location::find($id);
        $location->update($request->validate($this->rules));
        broadcast(new EntryUpdated('locations', (new LocationResource($location))->resolve()));
        return $location;
    }

    public function destroy($id)
    {

    }
}