<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourStopsalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_stopsales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_id')->unsigned();
            $table->integer('tour_variation_id')->unisigned();
            $table->integer('location_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->date('date');
            $table->boolean('soft_stopsale')->default(false)->comment('"Soft" stop sale, seller can call operationists before sale');
            $table->string('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_stopsales');
    }
}
