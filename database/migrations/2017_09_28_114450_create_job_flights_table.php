<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_flights', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id')->unsigned();
            $table->integer('operator_id')->unsigned()->nullable();
            $table->char('flight_id', 20)->index()->nullable();
            $table->timestampTz('flight_time')->nullable();
            $table->char('airport_code', 10)->nullable();
            $table->char('terminal', 10)->nullable();
            $table->tinyInteger('adults')->unsigned();
            $table->tinyInteger('childs')->unsigned();
            $table->tinyInteger('infants')->unsigned();
            $table->string('notes', 1000)->nullable();
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_job_flights');
    }
}
