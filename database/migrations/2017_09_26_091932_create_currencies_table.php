<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->char('id', 3)->comment('ISO 4217 Currency Codes');
            $table->string('name', 100);
            $table->char('symbol', 10)->comment('Currency sign like $');
            $table->boolean('main');
            $table->boolean('active')->default(true);
            $table->tinyInteger('sort_order', 3)->unsigned()->default(99);
            $table->timestampsTz();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
