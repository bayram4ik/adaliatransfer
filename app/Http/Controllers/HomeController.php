<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;
use App\Service;
use App\Vehicle;
use App;
class HomeController extends Controller
{
    public function index()
    {
        $hasDirections = Location::whereHas('directions')
                             ->orWhere(function($query){
                                return $query->whereHas('returnDirections');
                             })
                             ->with(['descendants' => function($query){
                                return $query->whereIn('type', [
                                    'destination',
                                    'subdestination',
                                    'hotel'
                                ]);
                             }])
                             ->get();

        $locations = collect([]);
        foreach($hasDirections as $location){
            if($location->type !== 'hotel'){
                $location['trans'] = $location->cyrr;
            }
            $locations->push($location);
            foreach($location->descendants as $l){
                $locations->push($l);
                $l['trans'] = $l->cyrr;
            }
        }

        $services = Service::get()->keyBy('slug');
        return view('home', compact('locations', 'services'));
    }
    public function changeCurrency()
    {
        session(['currency' => request('currency')]);
        return back();
    }
    public function changeLanguage()
    {
        App::setLocale(request('language'));
        session(['locale' => request('language')]);
        return back();
    }
}
