<?php
use Illuminate\Http\Request;
Route::namespace('Api')
	//->middleware(['api'])
	->group(function () {
		Route::post('/vehicles/upload', 'VehiclesController@upload');
		Route::delete('/vehicles/deleteimage', 'VehiclesController@deleteimage');
		Route::post('/users/upload', 'UsersController@upload');
		Route::delete('/users/deleteimage', 'UsersController@deleteimage');
		Route::apiResource('users',  	 	'UsersController');
		Route::apiResource('agencies',  	'AgenciesController');
		Route::apiResource('reservations',  'ReservationsController');
		Route::apiResource('locations',  	'LocationsController');
		Route::apiResource('vehicles', 	 	'VehiclesController');
		Route::apiResource('currencies', 	'CurrenciesController');
		Route::apiResource('directions', 	'DirectionsController');
	});