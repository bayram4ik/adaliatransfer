<h2>Yeni Acenta oluşturuldu</h2>

<div>
Yetkili adı: {{ $partner->name }}<br>
Acenta ismi: {{ $partner->details['agency_name'] }}<br>
Acenta adresi: {{ $partner->details['agency_address'] }}<br>
Acenta telefonu: {{ $partner->details['agency_phone'] }}<br>
E-mail: {{ $partner->email }}<br>
<br>
Oluşturma tarihi: {{ $partner->created_at->format('d F Y H:i') }}

</div>