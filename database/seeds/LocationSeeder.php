<?php

use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    public function run()
    {
    	Db::table('locations')->insert([
        	'id'   => 1,
        	'name' => 'Turkey',
        	'code' => 'TR',
        	'type' => 'country'
        ]);
        Db::table('locations')->insert([
        	'id'   => 2,
        	'name' => 'Antalya',
        	'code' => 'ANT',
        	'type' => 'destination',
        	'parent_id' => 1,
        	'_rgt' => 1
        ]);
        Db::table('locations')->insert([
        	'id'   => 3,
        	'name' => 'Kemer',
        	'code' => 'KMR',
        	'type' => 'destination',
        	'parent_id' => 2,
        	'_rgt' => 2
        ]);
        Db::table('locations')->insert([
        	'id'   => 4,
        	'name' => 'Belek',
        	'code' => 'BLK',
        	'type' => 'destination',
        	'parent_id' => 2,
        	'_rgt' => 2
        ]);
        Db::table('locations')->insert([
        	'id'   => 5,
        	'name' => 'Side',
        	'code' => 'SID',
        	'type' => 'destination',
        	'parent_id' => 2,
        	'_rgt' => 2
        ]);
        Db::table('locations')->insert([
        	'id'   => 6,
        	'name' => 'Alanya',
        	'code' => 'ALY',
        	'type' => 'destination',
        	'parent_id' => 2,
        	'_rgt' => 2
        ]);
        Db::table('locations')->insert([
        	'id'   => 7,
        	'name' => 'Antalya Airport',
        	'code' => 'AYT',
        	'type' => 'airport',
        	'parent_id' => 2,
        	'_rgt' => 2
        ]);
        Db::table('locations')->insert([
            'name' => 'T1',
            'code' => 'T1',
            'type' => 'terminal',
            'parent_id' => 7,
            '_rgt' => 3
        ]);
        Db::table('locations')->insert([
            'name' => 'T2',
            'code' => 'T2',
            'type' => 'terminal',
            'parent_id' => 7,
            '_rgt' => 3
        ]);

        Db::table('locations')->insert([
        	'id'   => 10,
        	'name' => 'Mahmutlar',
        	'code' => null,
        	'type' => 'subdestination',
        	'parent_id' => 6,
        	'_rgt' => 3
        ]);

        Db::table('locations')->insert([
        	'name' => 'Mahmutlar lojman',
        	'code' => null,
        	'type' => 'meetingpoint',
        	'parent_id' => 10,
        	'_rgt' => 4
        ]);

        Db::table('locations')->insert([
        	'name' => 'Gold Twins Suit Hotel',
        	'code' => null,
        	'type' => 'hotel',
        	'parent_id' => 10,
        	'_rgt' => 5
        ]);
        Db::table('locations')->insert([
        	'name' => 'White Gold Hotel & Spa',
        	'code' => null,
        	'type' => 'hotel',
        	'parent_id' => 10,
        	'_rgt' => 5
        ]);

    }
}
