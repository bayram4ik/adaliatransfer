@extends('layouts.main')
@section('title', trans('frontend.contact-us'))
@push('scripts')
<script>
    var app = new Vue({
        el: '#app',
        mixins: [currencyMixin]
    });
</script>
@endpush
@section('content')
<header class="page-header" style="background-image: url(assets/images/_inner-bg.jpg);">
		<div class="container">
			<ol class="bread">
				<li>
					<a href="{{ route('home') }}"><span><i class="fa fa-home"></i></span></a>
				</li>
				<li class="divider"><span>//</span></li>
				<li>
					<span>{{ trans('frontend.contacts') }}</span>
				</li>
			</ol>
			<h1>{{ trans('frontend.contacts') }}</h1>
		</div>
	</header>

	<section id="page-contacts">
		<div class="container">

			<div class="row">
			<div class="col-lg-12">
				@if(session('success') == true)
					<div class="alert alert-yellow">
						<div class="header"><span class="fa fa-info-circle"></span> {{ trans('frontend.success') }}</div>
						<p>{{ session('message') }}</p>
					</div>
				@endif
			</div>

				<div class="col-lg-6 col-md-6">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-6">

							<h2 class="spanned"><span>{{ trans('frontend.operating-hours') }}:</span> 24/7</h2>
							<p>{{ $settings->about_us_short }}</p>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-6">
							<ul class="address">
								<li class="large">
									<span class="fa fa-phone"></span>
									<a href="tel://{{ $settings->company_phone }}">
										{{ $settings->company_phone }}
									</a>
								</li>
								<li><span class="fa fa-email"></span>
									<a href="mailto:{{ $settings->company_email }}">
										{{ $settings->company_email }}
									</a>
								</li>
								<li><span class="fa fa-map-marker"></span>{!! $settings->company_address !!}</li>
							</ul>
						</div>
						<div class="col-lg-12 col-sm-12">
							<strong>{{ trans('frontend.social') }}:</strong>
							<ul class="social social-big">
								<li><a href="{{ $settings->social->fb }}" class="social-fb fa fa-facebook"></a></li>
								<li><a href="#" class="social-inst fa fa-instagram"></a></li>
							</ul>
							<a href="{{ route('transfers') }}" class="btn btn-black-bordered btn-lg">
								{{ trans('frontend.book-transfer-online') }}
							</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 contact-form">

					<form class="form form-sm" method="post" action="{{ route('send-message') }}">
						{!! csrf_field() !!}
						<h3 class="aligncenter">{{ trans('frontend.send-message') }}</h3>
						<div class="form-group">
							<label>{{ trans('frontend.your-name') }} <span class="red">*</span></label>
							<input type="text" id="name" name="name" placeholder="{{ trans('frontend.your-name') }}" class="required">
						</div>
						<div class="form-group">
							<label>E-mail <span class="red">*</span></label>
							<input type="text" id="email" name="email" placeholder="E-mail" class="required">
						</div>
						<div class="form-group">
							<label>{{ trans('frontend.message') }} <span class="red">*</span></label>
							<textarea id="text" name="message" placeholder="{{ trans('frontend.enter-message') }}" class="required"></textarea>
							<input type="hidden" id="cap" name="cap" value="" />
						</div>
						<input type="submit" name="send" value="{{ trans('frontend.send') }}" class="btn btn-yellow aligncenter btn-lg">
					</form>
				</div>
			</div>
		</div>
	</section>
    <div id="map" data-lat="36.8575057" data-lng="30.7613941" data-zoom="14"></div>


@endsection