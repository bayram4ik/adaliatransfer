@extends('layouts.main')
@section('title', trans('frontend.transfer-from') .' '. $fromLocation->name .' '. trans('frontend.to') .' '. $toLocation->name)
@section('metadescr', trans('frontend.transfers-metadesc', ['from' => $fromLocation->name, 'to' => $toLocation->name]))
@push('scripts')
<script>
	var app = new Vue({
		el: '#app',
		mixins: [currencyMixin],
		data: {
			distance: {!! $distance !!},
			champagnePrice: 30,
			bouquetPrice: 20,
			champagne: false,
			bouquet: false,
			transfers: {!! json_encode($transfers) !!},
		},
		methods: {
			calculatePrice: function(type){
				this.transfers.map(t => {
					if(this[type] === true)
						t.price.price = parseFloat(t.price.price)+this[type+'Price']
					else
						t.price.price = parseFloat(t.price.price)-this[type+'Price']
				})
			},
			redirect: function(e){
				let path = e.target.href
				if(this.champagne || this.bouquet){
					path += '?'
				}
				if(this.champagne){
					path += 'services[]=champagne'
					if(this.bouquet){
						path += '&'
					}
				}
				if(this.bouquet){
					path += 'services[]=bouquet'
				}
				window.location.href = path
			}
		}
	})
</script>
@endpush
@section('content')
	<header class="page-header">
		<div class="container">
			<ol class="bread">
				<li>
					<a href="{{ route('home') }}"><span><i class="fa fa-home"></i></span></a>
				</li>
				<li class="divider"><span>//</span></li>
				<li>
					<span>{{ trans('frontend.transfers') }}</span>
				</li>
			</ol>
			<h1 style="font-size: 30px;">{{ trans('frontend.transfer-from') }} {{ $fromLocation->name }} {{ trans('frontend.to') }} {{ $toLocation->name }}</h1>
			<center v-if="distance && distance.rows && distance.rows[0] && distance.rows[0].elements[0].duration && distance.rows[0].elements[0].distance && distance.rows[0].elements[0].status !== 'NOT_FOUND'">
				<strong>{{ trans('frontend.duration') }}:</strong> ~<span v-text="distance.rows[0].elements[0].duration.text"></span>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<strong>{{ trans('frontend.distance') }}</strong>: ~<span v-text="distance.rows[0].elements[0].distance.text"></span>
			</center>
		</div>
	</header>

	<div class="container">

	<form method="post" action="{{ route('redirectToTransfers') }}">
				{{ csrf_field() }}
				<div class="row" style="padding: 0;">
					<div class="col-md-4 col-md-offset-1">
						<div class="form-group">
							<select class="form-control" style="height: 40px !important;" name="from" id="from" v-selectize placeholder="{{ trans('frontend.from-dots') }}">
								<option>&nbsp;</option>
								<optgroup label="{{ trans('frontend.airports') }}">
									@foreach($locations->where('type', 'airport') as $location)
									<option value="{{ $location->slug }}" @if(request('from', session('from')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
								<optgroup label="{{ trans('frontend.destinations') }}">
									@foreach($locations->where('type', 'destination') as $location)
									<option value="{{ $location->slug }}" @if(request('from', session('from')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
								<optgroup label="{{ trans('frontend.locations') }}">
									@foreach($locations->where('type', 'subdestination') as $location)
									<option value="{{ $location->slug }}" @if(request('from', session('from')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
								<optgroup label="{{ trans('frontend.hotels') }}">
									@foreach($locations->where('type', 'hotel')->sortBy('name') as $location)
									<option value="{{ $location->slug }}" @if(request('from', session('from')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
<select v-selectize class="form-control" style="height: 40px !important;" name="to" id="to" placeholder="{{ trans('frontend.to-dots') }}">
								<option>&nbsp;</option>
								<optgroup label="{{ trans('frontend.airports') }}">
									@foreach($locations->where('type', 'airport') as $location)
									<option value="{{ $location->slug }}" @if(request('to', session('to')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
								<optgroup label="{{ trans('frontend.destinations') }}">
									@foreach($locations->where('type', 'destination') as $location)
									<option value="{{ $location->slug }}" @if(request('to', session('to')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
								<optgroup label="{{ trans('frontend.locations') }}">
									@foreach($locations->where('type', 'subdestination') as $location)
									<option value="{{ $location->slug }}" @if(request('to', session('to')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
								<optgroup label="{{ trans('frontend.hotels') }}">
									@foreach($locations->where('type', 'hotel')->sortBy('name') as $location)
									<option value="{{ $location->slug }}" @if(request('to', session('to')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
							</select>
						</div>
					</div>
					<div class="col-md-1">
					<button type="submit" class="btn btn-yellow btn-bg-dark btn-lg" style="margin: 0 10px; padding: 7px 60px;">
						<i class="fa fa-search"></i>
					</button>

					</div>
				</div>
			</form>


		<div class="inner inner-two-col row" style="margin-bottom: 15px;">
			<div class="col-lg-10 col-lg-offset-1">
				<div id="comments" class="comments-area">
						<div class="comments-ol">
							<ol class="comment-list" style="margin-top:4px;">
								<li class="fw-feedback byuser comment-author-admin bypostauthor even thread-even depth-1 comment_item" v-for="transfer in transfers" v-if="!isNaN(transfer.price.price) && parseInt(transfer.price.price) > 0">
									<div class="comment-single">
										<div class="comment-content" style="padding-left: 0px;">
											<div class="comment-info row">
												<div class="col-md-8">
													<span class="comment-author" style="font-size: 20px;color: #777;" v-text="`${transfer.vehicle.name} ${transfer.vehicle.pax_capacity} {{ trans('frontend.pax') }}`">
													</span>
													<span class="comment-date" style="font-size: 12px;">
														<span class="comment_date_label">
															<i class="fa fa-users"></i> {{ trans('frontend.passengers') }}:
														</span>
														<span class="comment_date_value"
															  v-text="transfer.vehicle.pax_capacity">
														</span>
													</span>
													<span class="comment-time" style="font-size: 12px;">
														<i class="fa fa-suitcase"></i> {{ trans('frontend.baggages') }}:
 												<span v-text="transfer.vehicle.baggage_capacity"></span>
													</span>
												</div>
											<div class="col-md-4">
													<span class="pull-right" style="font-size: 20px; color: #444; font-weight: bold;" v-text="convert(transfer.price.price, transfer.price.currency)">

													</span><br />
													<span class="pull-right" style="font-size: 12px; font-weight: normal;color: #aaa;">
															({{ trans('frontend.old-price') }}:
																<span v-text="convert((parseFloat(transfer.price.price)+5), transfer.price.currency)"></span>
															)
														</span>
												</div>
											</div>
											<div class="comment_text_wrap">
												<div class="comment-text row">
													<div class="col-md-8">
														<img :src="transfer.vehicle.attachments[0]['url']" width="190" />
														<span style="padding-left: 20px;"
															  v-text="transfer.vehicle.models">
														</span>
													</div>
													<div class="col-md-4 col-xs-12" style="height: 80px;">
														<a @click.prevent="redirect" :href="transfer.url" class="btn btn-yellow btn-bg-dark pull-right" style="text-transform: none;margin: 0 10px; padding: 12px 60px;position: absolute;bottom:0px;right:0px;">
															{{ trans('frontend.book-transfer') }}
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ol>
						</div>
					</div>
			</div>

			{{--
			<div class="col-lg-3">

				<div class="widget-area" role="complementary" style="padding-top: 40px;">
				<h6 class="aligncenter" style="font-size: 16px; font-weight: thin; margin: 0px;">Additional services</h6>
				<small class="aligncenter">(click to include or exclude)</small>
				<ul class="nav nav-pills nav-stacked">
							<li role="presentation" style="border: 1px solid #FFC61A;">
								<a href="#" @click.prevent="champagne = !champagne;calculatePrice('champagne')" :class="{ selected: champagne }">
									<center>
										<span class="label label-warning">Champagne</span>
										<img src="{{ asset('images/champagne.png') }}" width="100">
									</center>
									<span class="label label-success">30$</span>
								</a>
							</li>
							<li role="presentation" style="border: 1px solid #FFC61A;">
								<a href="#" @click.prevent="bouquet = !bouquet;calculatePrice('bouquet')" :class="{ selected: bouquet }">
									<center>
										<span class="label label-warning">Bouquet of flowers</span>
										<img src="{{ asset('images/buket.png') }}" width="100">
									</center>
									<span class="label label-success">20$</span>
								</a>
							</li>
						</ul>
				</div>
			</div>
			--}}
		</div>
		<section id="tariffs" class="bg-white other-locations">
			<h4 style="margin-top: 0px;">{{ trans('frontend.other-transfers') }} {{ $fromLocation->name }}</h4>
			<div class="row">
				@foreach($otherLocations as $location)
				@if(array_key_exists('price',collect($location->prices)->sortBy('price')->first()))
				<div class="col-md-4 col-sm-6" style="margin-bottom: 30px;">
					<div class="item matchHeight" style="padding: 10px 20px;text-align: left;">
						<a href="{{ route('transfers', ['from' => $fromLocation->slug, 'to' => $location->end_location->slug]) }}">
							{{ trans('frontend.to') }} {{ $location->end_location->name }}
							<span class="label label-default pull-right" style="padding: .3em 2em .3em;font-size:100%;">
								{{ trans('frontend.from') }}
								<span v-text="convert({{ (int)collect($location->prices)->sortBy('price')->first()['price'] }}, '{{ collect($location->prices)->sortBy('price')->first()['currency'] }}')">
								</span>
							</span>
						</a>
					</div>
				</div>
				@endif
				@endforeach
			</div>
		</section>
		</div>
	</div>
@endsection