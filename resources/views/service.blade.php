@extends('layouts.main')
@section('title', trans('frontend.our-services'))
@push('scripts')
<script>
    var app = new Vue({
        el: '#app',
        mixins: [currencyMixin]
    });
</script>
@endpush
@section('content')

<header class="page-header" style="background-image: url({{ asset('assets/images/_inner-bg.jpg') }});">
        <div class="container">
            <ol class="bread">
                <li>
                    <a href="{{ route('home') }}"><span><i class="fa fa-home"></i></span></a>
                </li>
                <li class="divider"><span>//</span></li>
                <li>
                    <a href="{{ route('services') }}">
                        <span>{{ trans('frontend.our-services') }}</span>
                    </a>
                </li>
            </ol>
            <h1>{{ $service->name }}</h1>
        </div>
    </header>

    <div class="container">
        <div class="inner row">
            <div class="col-lg-8 text-page">
                <div class="blog-post text-page">
                    {!! $service->content !!}
                </div>
            </div>
            <div class="col-lg-4">
                <h4 style="margin: 0px">{{ trans('frontend.ayt-transfers') }}</h4>
                <section id="tariffs" class="bg-white other-locations">
                @foreach($locations as $location)
                    <div class="item matchHeight" style="padding: 10px 20px;text-align: left;margin-bottom: 10px;">
                        <a href="{{ route('transfers', ['from' => $location->start_location->slug, 'to' => $location->end_location->slug]) }}">
                            {{ $location->start_location->name }} <i class="fa fa-long-arrow-right"></i> {{ $location->end_location->name }}
                            <span class="label label-default pull-right" style="padding: .3em .5em .3em;font-size:85%;">
                                {{ trans('frontend.from') }}
                                <span v-text="convert({{ collect($location->prices)->sortBy('price')->first()['price'] }}, '{{ collect($location->prices)->sortBy('price')->first()['currency'] }}')">
                                </span>
                            </span>
                        </a><br>
                    </div>
                @endforeach
            </section>
            </div>
        </div>
    </div>

@endsection