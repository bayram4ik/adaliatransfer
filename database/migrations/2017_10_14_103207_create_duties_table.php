<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDutiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('duties', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['arrival', 'departure', 'tour', 'sub', 'ferry']);
            $table->integer('start_location_id')->unsigned();
            $table->integer('end_location_id')->unsigned();
            $table->integer('km')->unsigned()->default(0);
            $table->integer('mins')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('duties');
    }
}
