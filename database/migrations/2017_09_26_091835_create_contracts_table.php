<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 255);
            $table->decimal('opr_contr', 8, 2)->default(0.00);
            $table->enum('opr_contr_unit', ['pp', 'pc', 'ps'])->default('pp')->comment('pp - per person, pc - per cent, ps - per season');
            $table->decimal('pp_add_contr', 8, 2)->default(0.00);
            $table->enum('pp_add_contr_unit', ['pp', 'pc', 'ps'])->default('pp');
            $table->decimal('per_sale_kb', 8, 2)->default(0.00)->comment('Per sale kickback');
            $table->enum('per_sale_kb_unit', ['pp', 'pc', 'ps'])->default('pp');
            $table->decimal('tech_contr', 8, 2)->default(0.00);
            $table->enum('tech_contr_unit', ['pp', 'pc', 'ps'])->default('pp');
            $table->decimal('adv_contr', 8, 2)->default(0.00);
            $table->enum('adv_contr_unit', ['pp', 'pc', 'ps'])->default('pp');
            $table->decimal('seller_bonus', 8, 2)->default(0.00);
            $table->enum('seller_bonus_unit', ['pp', 'pc', 'ps'])->default('pp');
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
