@servers(['web' => '-A -p 50683 -l root adaliatransfer.com'])

@task('deploy', ['on' => 'web'])
    cd /var/www/localhost
    git reset --hard
    git pull
    npm run build
@endtask

@finished
    @slack('https://hooks.slack.com/services/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', '#general')
@endfinished