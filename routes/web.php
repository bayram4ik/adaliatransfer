<?php
Route::group(['prefix' => LaravelLocalization::setLocale(),
              'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]], function(){
    Route::get('/', 			'HomeController@index')->name('home');
    Route::post('/', 			'TransfersController@redirectToTransfers')->name('redirectToTransfers');
    Route::get('/transfers/{from?}/{to?}', 'TransfersController@index')->name('transfers');
    Route::get('/book/{from?}/{to?}/{vehicle?}', 		'TransfersController@book')->name('book');
    Route::post('/book', 		'TransfersController@makeBook')->name('make-book');
    Route::post('/payment', 'TransfersController@payment')->name('payment');

    Route::get('/howitworks',       'AboutController@howitworks')->name('howitworks');
    Route::get('/fleet', 		'AboutController@fleet')->name('fleet');
    Route::get('/terms', 		'AboutController@terms')->name('terms');
    Route::get('/partner', 		'AboutController@partner')->name('partner');
    Route::post('/register/partner',    'AboutController@registerPartner')->name('register-partner');
    Route::post('/sendmessage',    'AboutController@sendMessage')->name('send-message');
    Route::get('/contacts',		'AboutController@contacts')->name('contacts');
    Route::get('/faq', 			'AboutController@faq')->name('faq');
    Route::get('/reviews',   	'ReviewsController@index')->name('reviews');
    Route::get('/services/{type?}',   	'AboutController@services')->name('services');
    Route::get('/blog/{post?}', 'BlogController@index')->name('blog');

    Route::get('/change/currency/{currency}', 'HomeController@changeCurrency')->name('change-currency');
    Route::get('/change/language/{language}', 'HomeController@changeLanguage')->name('change-language');
});

Route::middleware(['auth'])->group(function () {

   	Route::get('/app/{vue_capture?}', 'BackendController@index')
   		 ->where('vue_capture', '[\/\w\.-]*')
		 ->name('backend');

   	Route::get('logout', 'Auth\LoginController@logout');
});

Auth::routes();