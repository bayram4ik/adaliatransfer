@extends('layouts.main')
@push('scripts')
<script>
    var app = new Vue({
        el: '#app',
        mixins: [currencyMixin]
    });
</script>
@endpush
@section('content')

<header class="page-header" style="background-image: url(assets/images/_inner-bg.jpg);">
		<div class="container">
			<ol class="bread">
				<li>
					<a href="{{ route('home') }}"><span><i class="fa fa-home"></i></span></a>
				</li>
				<li class="divider"><span>//</span></li>
				<li>
					<span>{{ trans('frontend.reviews') }}</span>
				</li>
			</ol>
			<h1>{{ trans('frontend.reviews') }}</h1>
		</div>
	</header>

	<div class="container">
		<div id="testimonials-list" class="inner">
			<div class="row masonry">
								<div class="col-sm-4 item">
					<div class="inner">
						<div class="text">
							<p>Все отлично,
встретили с табличной, отвезли.</p>
						</div>
						<div class="quote">
							<span class="fa fa-quote-left"></span>
							<div class="name">Виталий Березин</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 item">
					<div class="inner">
						<div class="text">
							<p>Отличный сервис, хороший водитель. Очень довольна!!!</p>
						</div>
						<div class="quote">
							<span class="fa fa-quote-left"></span>
							<div class="name">BARAN VERA</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 item">
					<div class="inner">
						<div class="text">
							<p>Все отлично!!! Встретили. Машина была на класс выше чем заказал. Довезти без доплаты. Отличный сервис!!</p>
						</div>
						<div class="quote">
							<span class="fa fa-quote-left"></span>
							<div class="name">DMITRII BELIAEV</div>
						</div>
					</div>
				</div>
								<div class="col-sm-4 item">
					<div class="inner">
						<div class="text">
							<p>Всё было организованно на отлично. Несмотря на задержку рейса,
нас ожидал водитель. Машина была на класс выше ( без доплаты).
Мы очень довольны сервисом.</p>
						</div>
						<div class="quote">
							<span class="fa fa-quote-left"></span>
							<div class="name">Kristina Bradu</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 item">
					<div class="inner">
						<div class="text">
							<p>Заказал микроавтобус (7 мест) на двух человек за день до прилета. В аэропорту водитель уже встречал с табличкой, помог с багажом. Машина была шикарная. Пока ехали до Белека устроились удобно в креслах и подремали. </p>
						</div>
						<div class="quote">
							<span class="fa fa-quote-left"></span>
							<div class="name">Сергей</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 item">
					<div class="inner">
						<div class="text">
							<p>Водитель приехал вовремя в отель даже немного раньше,вообщем когда мы вышли к выходу он был уже на месте. Поездка прошла отлично. Рекомендую.</p>
						</div>
						<div class="quote">
							<span class="fa fa-quote-left"></span>
							<div class="name">natalija</div>
						</div>
					</div>
				</div>
								<div class="col-sm-4 item">
					<div class="inner">
						<div class="text">
							<p>Все очень понравилось хорошая машина приветливый Водитель. Довёз с комфортом!</p>
						</div>
						<div class="quote">
							<span class="fa fa-quote-left"></span>
							<div class="name">Valentin Hatuntsev</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 item">
					<div class="inner">
						<div class="text">
							<p>Спасибо большое, остались очень довольны! Будем и дальше пользоваться Вашими услугами и рекомендовать их! Спасибо!</p>
						</div>
						<div class="quote">
							<span class="fa fa-quote-left"></span>
							<div class="name">Anna Balashkevych</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 item">
					<div class="inner">
						<div class="text">
							<p>Очень благодарны за трансфер! отличная машина и водитель! дождался нас, несмотря на очень длительную задержку (из-за выдачи багажа)! рекомендую как надежный и качественный сервис!!!!</p>
						</div>
						<div class="quote">
							<span class="fa fa-quote-left"></span>
							<div class="name">Aleksandra Peshkova</div>
						</div>
					</div>
				</div>
								<div class="col-sm-4 item">
					<div class="inner">
						<div class="text">
							<p>Заказывали трансфер из аэропорта Анталия в Сиде и обратно. Впечатления самые положительные! Всё быстро, чётко, своевременно. Водитель встречал с табличкой на выходе с аэропорта, помог донести и загрузить багаж. Автомобили в прекрасном состоянии, оба раза были классом выше, чем заказывали. Денег взяли строго в соответствии с трансфером. Обязательно воспользуемся Вашими услугами в этом году, а так же рекомендуем друзьям.</p>
						</div>
						<div class="quote">
							<span class="fa fa-quote-left"></span>
							<div class="name">Алексей Юдаков</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 item">
					<div class="inner">
						<div class="text">
							<p>Всегда рекомендую вас своим друзьям. Очень хорошая организация оказания услуг. Приятные цены. Водители относятся к клиенту с уважением. Встреча в аэропорту с табличкой. Все быстро и четко. Дополнительных денег вытянуть не пытаются. Автопарк в хорошем состоянии.</p>
						</div>
						<div class="quote">
							<span class="fa fa-quote-left"></span>
							<div class="name">Ольга</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 item">
					<div class="inner">
						<div class="text">
							<p>Несмотря на то, что рейс из Москвы задержали почти на 2 часа, водитель в Анталье дождался нас. Помог погрузить вещи и с ветерком доставил в отель. Никаких нареканий, все прекрасно. Спасибо большое за сервис.</p>
						</div>
						<div class="quote">
							<span class="fa fa-quote-left"></span>
							<div class="name">Timofeeva Julia</div>
						</div>
					</div>
				</div>


			</div>
			{{--
			<div class="paging-navigation">
				<hr>
				<div class="pagination">
					<a href="#" class="prev disabled"><span class="fa fa-caret-left"></span> prev</a>
					<a href="#" class="page-numbers current">1</a>
					<a href="#" class="page-numbers">2</a>
					<a href="#" class="page-numbers">3</a>
					<a href="#" class="page-numbers">4</a>
					<a href="#" class="next">next <span class="fa fa-caret-right"></span></a>
				</div>
			</div>
			--}}
		</div>

	</div>


@endsection