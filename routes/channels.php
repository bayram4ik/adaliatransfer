<?php

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

/*Broadcast::channel('app', function ($user) {
    return $user;
});*/

Broadcast::channel('users', function ($user) {
    return $user;
});
Broadcast::channel('updates', function ($user) {
    return $user;
});
