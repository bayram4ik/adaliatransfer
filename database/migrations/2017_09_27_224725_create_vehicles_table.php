<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->char('id', 10);
            $table->boolean('active');
            $table->integer('company_id')->unsigned();
            $table->enum('type', ['mini', 'midi', 'bus']);
            $table->integer('vehicle_brand_id')->unsigned();
            $table->integer('vehicle_model_id')->unsigned();
            $table->smallInteger('model_year')->unsigned()->nullable();
            $table->enum('fuel_type', ['diesel', 'benzin', 'gas'])->default('diesel');
            $table->tinyInteger('capacity')->unsigned();
            $table->decimal('km_lt', 4, 2)->default(0.00);
            
            $table->date('hired_at')->nullable();
            $table->date('dismissed_at')->nullable();
            $table->jsonb('contacts')->nullable()->comment('owner_company, person, telephone, fax, invoice_address');
            $table->integer('start_km')->unsigned()->default(0);
            $table->integer('end_km')->unsigned()->default(0);

            $table->date('traffic_ins_ends_at')->nullable();
            $table->date('seat_ins_ends_at')->nullable();
            $table->date('other_ins_ends_at')->nullable();
            $table->string('other_ins_name')->nullable();

            $table->date('inspection_ends_at')->nullable();

            $table->integer('driver_id')->unsigned()->nullable();

            $table->string('tracking_soft_serial', 50)->nullable();
            $table->string('tracking_soft_sim', 50)->nullable();
            $table->string('wifi_device_serial')->nullable();

            $table->string('licence_table_serial')->nullable()->comment('In Turkey TURSAB');

            $table->timestampsTz();

            $table->primary('id');
            $table->unique(['id', 'type', 'company_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
