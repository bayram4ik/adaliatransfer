import Vuex from 'vuex'
import Vue from 'vue'
import { set, get, findIndex } from 'lodash'
import { Message } from 'element-ui'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		errors: [],
		modal: false,
		loading: false,
		filters: {
			prices: {}
		},
		locations: [],
		currencies: [],
		vehicles: [],
		directions: [],
		agencies: [],
		users: [],
		reservations: [],
		userTypes: {
			admin:    { name: 'Yönetici' },
			agency:   { name: 'Acenta' },
			customer: { name: 'Musteri' }
		},
		agencyStatuses: {
			new: 	 { name: 'Yeni' },
			pending: { name: 'Evraklar bekleniyor' },
			active:  { name: 'Aktif' },
			denied:  { name: 'Reddedildi' }
		}
	},
	getters: {
		errors: 		state => state.errors,
		filters: 		state => state.filters,
		modal: 			state => state.modal,
		loading: 		state => state.loading,
		directions:		state => state.directions,
		locations: 		state => state.locations,
		airports: 		state => state.locations.filter(l => l.type == 'airport'),
		countries: 		state => state.locations.filter(l => l.type == 'country'),
		cities: 		state => state.locations.filter(l => l.type == 'city'),
		hotels: 		state => state.locations.filter(l => l.type == 'hotel'),
		destinations:   state => state.locations.filter(l => l.type == 'destination'),
		subdestinations: state => state.locations.filter(l => l.type == 'subdestination'),
		currencies: 	state => state.currencies,
		reservations: 	state => state.reservations,
		vehicles: 		state => state.vehicles,
		users: 			state => state.users,
		agencies: 		state => state.agencies,
		agencyStatuses: state => state.agencyStatuses
	},
	mutations: {
		fill(state, obj){
			set(state, obj[0], obj[1])
		},
		add(state, obj){
			get(state, obj[0]).push(obj[1])
		},
		replace(state, obj){
			let items = get(state, obj[0])
			if(obj[1] !== undefined && obj[1] !== null && obj[1].hasOwnProperty('id')){
				let index = items.findIndex(item => item.id == obj[1].id)
				state[obj[0]].splice(index, 1, obj[1])
			}else{
				set(state, obj[0], obj[1])
			}
		},
		remove(state, obj){
			state[obj[0]].splice(findIndex(state[obj[0]], { id: obj[1].id }), 1)
		}
	},
	actions: {
		fetch({ commit, dispatch }, obj) {
			var path = null
			var	params = null
			if(typeof obj === 'string'){
				path = obj
			}else{
				path = obj[0]
				params = obj[1]
			}
			commit('replace', ['loading', true])
			return new Promise((resolve, reject) => {
				axios.get(`/api/${path}`, { params })
					 .then(res => {
					 	commit('fill', [path, res.data])
					 	commit('replace', ['loading', false])
					 	resolve(res)
					 })
					 .catch(errors => {
					 	dispatch('errors', errors.response.data)
					 	commit('replace', ['loading', false])
					 	reject(errors)
					 })
			})
		},
		store({ commit, dispatch }, obj){
			commit('replace', ['loading', true])
			return new Promise((resolve, reject) => {
				axios.post(`/api/${obj[0]}`, obj[1])
					 .then(res => {
					 	commit('replace', ['loading', false])
					 	resolve(res)
					 })
					 .catch(errors => {
					 	dispatch('errors', errors.response.data)
					 	commit('replace', ['loading', false])
					 	reject(errors)
					 })
			})
		},
		update({ commit, dispatch }, obj){
			commit('replace', ['loading', true])
			return new Promise((resolve, reject) => {
				axios.put(`/api/${obj[0]}/${obj[1]['id']}`, obj[1])
					 .then(res => {
					 	commit('replace', ['loading', false])
					 	resolve(res)
					 })
					 .catch(errors => {
					 	dispatch('errors', errors.response.data)
					 	commit('replace', ['loading', false])
					 	reject(errors)
					 })
			})
		},
		destroy({ commit, dispatch, state }, obj){
			commit('replace', ['loading', true])
			return new Promise((resolve, reject) => {
				if(obj[1] !== undefined && obj[1] !== null){
					let item = obj[1]
					if(item !== undefined && item !== null && item.hasOwnProperty('id')){
						axios.delete(`/api/${obj[0]}/${item.id}`)
							.then(res => {
								if(res.data.success){
									commit('remove', obj)
									commit('replace', ['loading', false])
									resolve(res)
								}
							})
							.catch(errors => {
								dispatch('errors', errors.response.data)
								commit('replace', ['loading', false])
								reject(errors)
							})
					}else{
						commit('remove', obj)
						commit('replace', ['loading', false])
						resolve(true)
					}
				}
			})
		},
		restore({ commit, dispatch }, obj){
			commit('replace', ['loading', true])
			return new Promise((resolve, reject) => {
				axios.post(`/api/${obj[0]}/restore/${obj[1]}`)
					 .then(res => {
					 	if(res.data.success){
					 		commit('replace', ['loading', false])
					 		resolve(res)
					 	}
					 })
					 .catch(errors => {
					 	dispatch('errors', errors.response.data)
					 	commit('replace', ['loading', false])
					 	reject(errors)
					 })
			})
		},
		change({ commit }, obj){
			commit('replace', ['loading', true])
			return new Promise((resolve, reject) => {
				commit('replace', obj)
				commit('replace', ['loading', false])
				resolve(obj)
			})
		},
		unset({ commit }, path){
			commit('replace', [path, []])
		},
		remove({ commit }, obj){
			if(typeof obj === 'string'){
				commit('replace', [obj, null])
			}else{
				commit('remove', obj)
			}
		},
		add({ commit }, obj){
			commit('add', obj)
		},
		toggle({ commit, state }, path){
			commit('replace', [path, !get(state, path)])
		},
		errors({ commit }, errors){
			commit('fill', ['errors', errors])
			Message({ message: errors.message, type: 'error' })
		}
	}
})