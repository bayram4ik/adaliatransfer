<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->boolean('excludable')->default(false)->comment('If true seller can exclude this service after sale');
            $table->integer('service_price_id')->unsigned();
            $table->tinyInteger('units')->default(0);
            $table->tinyInteger('adults')->default(0);
            $table->tinyInteger('childs')->default(0);
            $table->tinyInteger('infants')->default(0);
            $table->decimal('unit_price', 8, 2)->default(0.00);
            $table->decimal('adult_price', 8, 2)->default(0.00);
            $table->decimal('child_price', 8, 2)->default(0.00);
            $table->decimal('infant_price', 8, 2)->default(0.00);
            $table->decimal('total', 8, 2)->storedAs('(units*unit_price)+(adults*adult_price)+(childs*child_price)+(infants*infant_price)');
            $table->char('currency_id', 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_costs');
    }
}
