<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Company extends Model{
	use NodeTrait;

	protected $auditExclude = [
	    'created_at',
	    'updatet_at'
	];

	const types = ['company', 'supplier', 'operator', 'agency'];

	public function location(){
		return $this->belongsTo(Location::class);
	}

    public function scopeType($query, $type = 'company'){
    	if(in_array($type, self::types))
    		return $query->where('type', $type);
    }

    public function scopeOwn($query){
    	return $query->where('own', true);
    }


}
