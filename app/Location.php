<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Location extends Model{
	use HasSlug;
  
	protected $guarded = [];

    use \Kalnoy\Nestedset\NodeTrait;
	const types = ['destination','subdestination','district','city','country','place','hotel','airport'];

    public function scopeType($query, $type = 'destination'){
    	if(in_array($type, self::types))
    		return $query->where('type', $type);
	}
    public function getSlugOptions() : SlugOptions{
        return SlugOptions::create()
                          ->generateSlugsFrom('name')
                          ->saveSlugsTo('slug')
                          ->usingLanguage('tr')
                          ->slugsShouldBeNoLongerThan(100)
                          ->doNotGenerateSlugsOnUpdate();
    }
    public function getCyrrAttribute(){
        $en = array("Sch","sch",'Yo','Zh','Kh','Ts','Ch','Sh','Yu','ya','yo','zh','kh','ts','ch','sh','yu','ya','A','B','V','G','D','E','Z','I','Y','K','L','M','N','O','P','R','S','T','U','F','','Y','','E','a','b','v','g','d','e','z','i','y','k','l','m','n','o','p','r','s','t','u','f','','y','','e');
        $ru = array("Щ","щ",'Ё','Ж','Х','Ц','Ч','Ш','Ю','я','ё','ж','х','ц','ч','ш','ю','я','А','Б','В','Г','Д','Е','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Ь','Ы','Ъ','Э','а','б','в','г','д','е','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','ь','ы','ъ','э');
        return str_replace($en, $ru, $this->name);
    }
    public function directions(){
      return $this->hasMany(Direction::class, 'start_location_id');
    }
    public function returnDirections(){
      return $this->hasMany(Direction::class, 'end_location_id');
    }
}
