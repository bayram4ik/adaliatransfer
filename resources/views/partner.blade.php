@extends('layouts.main')
@section('title', trans('frontend.become-partner'))
@push('scripts')
<script>
    var app = new Vue({
        el: '#app',
        mixins: [currencyMixin]
    });
</script>
@endpush
@section('content')
    <section id="download" class="parallax partner" style="background-image: url({{ asset('assets/images/prtnr.jpg')}});">
        <div class="container">
            <h4 class="yellow" style="margin-top: 10px;">
                {{ trans('frontend.for-travel-agencies') }}
            </h4>
            <h2 class="h1" style="margin-bottom: 10px !important;">
                {{ trans('frontend.do-you-want-earn') }}
            </h2>
            <div class="row">
            <div class="col-lg-12">
                @if(session('success') == true)
                    <div class="alert alert-yellow">
                        <div class="header"><span class="fa fa-info-circle"></span> {{ trans('frontend.success') }}</div>
                        <p>{{ session('message') }}</p>
                    </div>
                @endif
            </div>
                <div class="col-md-4 col-sm-12">
                    <div class="items row">
                        <div class="col-md-2 visible-md visible-lg">
                            <span class="num">01.</span>
                        </div>
                        <div class="col-md-10">
                            <h5 class="yellow">
                                {{ trans('frontend.client-support') }}
                            </h5>
                        </div>
                    </div>
                    <div class="items row pt-0">
                        <div class="col-md-2 visible-md visible-lg">
                            <span class="num">02.</span>
                        </div>
                        <div class="col-md-10">
                            <h5 class="yellow">
                                {{ trans('frontend.invoice-print') }}
                            </h5>
                        </div>
                    </div>
                    <div class="items row pt-0">
                        <div class="col-md-2 visible-md visible-lg">
                            <span class="num">03.</span>
                        </div>
                        <div class="col-md-10">
                            <h5 class="yellow">
                                {{ trans('frontend.payment-methods') }}
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-md-push-4 col-sm-12">
                    <div class="items items-right row">
                        <div class="col-md-10">
                            <h5 class="yellow">
                                {{ trans('frontend.fix-prices') }}
                            </h5>
                        </div>
                        <div class="col-md-2 visible-md visible-lg">
                            <span class="num">04.</span>
                        </div>
                        <div class="col-md-10">
                            <h5 class="yellow">
                                {{ trans('frontend.prof-drivers') }}
                            </h5>
                        </div>
                        <div class="col-md-2 visible-md visible-lg">
                            <span class="num">05.</span>
                        </div>
                        <div class="col-md-10">
                            <h5 class="yellow">
                                {{ trans('frontend.stability') }}
                            </h5>
                        </div>
                        <div class="col-md-2 visible-md visible-lg">
                            <span class="num">06.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="services">
        <div class="container">
            <h2 class="h1">{{ trans('frontend.how-it-works') }}</h2>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-ms-6 matchHeight">
                    <div class="image">
                        <img src="{{ asset('assets/images/register-partner.png') }}" alt="Service" height="80">
                    </div>
                    <h5>{{ trans('frontend.partner-register') }}</h5>
                    <p>{{ trans('frontend.partner-register-desc') }}</p>
                </div>
                <div class="col-md-4 col-sm-6 col-ms-6 matchHeight">
                    <div class="image">
                        <img src="{{ asset('assets/images/book-transfers.png') }}" alt="Service" height="80">
                    </div>
                    <h5>{{ trans('frontend.make-clients-booking') }}</h5>
                    <p>{{ trans('frontend.make-clients-booking-desc') }}</p>
                </div>
                <div class="col-md-4 col-sm-6 col-ms-6 matchHeight">
                    <div class="image">
                        <img src="{{ asset('assets/images/make-money.png') }}" alt="Service" height="80">
                    </div>
                    <h5>{{ trans('frontend.get-profit') }}</h5>
                    <p>{{ trans('frontend.get-profit-desc') }}</p>
                </div>
            </div>
        </div>
    </section>

    <section class="form-taxi-short">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h4 class="yellow aligncenter">{{ trans('frontend.partner-has-questions') }}</h4>
                    <h3 class="aligncenter">{{ $settings->company_phone }}</h3>
                    <br>
                    <form action="{{ route('register-partner') }}" method="post">
                        {!! csrf_field() !!}
                        <div class="row form-with-labels">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="name" value="" placeholder="{{ trans('frontend.your-name') }}" required><span class="fa fa-user"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="agency_name" value="" placeholder="{{ trans('frontend.company-name') }}" required><span class="fa fa-building"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row form-with-labels">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="email" value="" placeholder="Email" required><span class="fa fa-envelope-open"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="agency_address" value="" placeholder="{{ trans('frontend.country-city')}}" required><span class="fa fa-map-marker"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row form-with-labels">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="agency_phone" value="" placeholder="{{ trans('frontend.phone') }}" required><span class="fa fa-phone"></span>
                                </div>
                            </div>
                        </div>

                        <input type="submit" value="{{ trans('frontend.become-partner') }}" class="btn btn-lg btn-black btn-white aligncenter">
                    </form>
                </div>
                <div class="col-md-5">
                    <div class="skills skills-inner" style="background-image: url({{ asset('assets/images/_homepage-1-bg.jpg') }})">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-ms-12">
                                <div class="item">
                                    <span id="countUp-1">{{ $stats['partners'] }}</span>
                                    {{ trans('frontend.partners') }}
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-ms-12">
                                <div class="item">
                                    <span id="countUp-2">{{ $stats['orders'] }}</span>
                                    {{ trans('frontend.orders') }}
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-ms-12">
                                <div class="item">
                                    <span id="countUp-3">{{ $stats['customers'] }}</span>
                                    {{ trans('frontend.customers') }}
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-ms-12">
                                <div class="item">
                                    <span id="countUp-4">{{ $stats['routes'] }}</span>
                                    {{ trans('frontend.routes') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection