<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Currency;

class CurrenciesController extends Controller
{

    public function index()
    {
        return Currency::orderBy('sort_order')->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
