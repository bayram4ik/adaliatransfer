<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
class Faq extends Model{
    use HasTranslations;
    public $translatable = ['question', 'answer'];
    public $guarded = [];
    public $timestamps = false;

    public function category(){
        return $this->belongsTo(FaqCategory::class);
    }
}
