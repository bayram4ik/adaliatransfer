<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['regular', 'private']);
            $table->integer('tour_id')->unsigned();
            $table->integer('tour_variation_id')->unsigned();
            $table->date('tour_date');
            $table->timeTz('pickup_time');
            $table->enum('pickup_point', ['reception', 'gate'])->default('reception');
            $table->integer('departure_location_id')->unsigned()
                  ->comment('Hotel or other location tourist will be picked up');

            $table->integer('seller_id')->unsigned()->comment('Guide id or in case of B2C sale, agent\'s ID');
            
            $table->integer('company_id')->unsigned()->comment('Tourists\'s operator or in case of B2C sale, agency ID');

            $table->string('seller_remarks', 250)->nullable();

            $table->json('customer_details')->nullable()
                  ->comment('Customer names, room number, email address (in case of credit card payment),
                             passport details in case of int.flights, visa confirmations, airticket confirmations,
                             shop reservation confirmation etc.');

            $table->tinyInteger('initial_units')->default(0);
            $table->tinyInteger('initial_adults')->default(0);
            $table->tinyInteger('initial_childs')->default(0);
            $table->tinyInteger('initial_infants')->default(0);

            $table->tinyInteger('canceled_units')->default(0);
            $table->tinyInteger('canceled_adults')->default(0);
            $table->tinyInteger('canceled_childs')->default(0);
            $table->tinyInteger('canceled_infants')->default(0);

            $table->tinyInteger('noshow_units')->default(0);
            $table->tinyInteger('noshow_adults')->default(0);
            $table->tinyInteger('noshow_childs')->default(0);
            $table->tinyInteger('noshow_infants')->default(0);

            $table->tinyInteger('units')->storedAs('(initial_units-canceled_units-noshow_units)');
            $table->tinyInteger('adults')->storedAs('(initial_adults-canceled_adults-noshow_adults)');
            $table->tinyInteger('childs')->storedAs('(initial_childs-canceled_childs-noshow_childs)');
            $table->tinyInteger('infants')->storedAs('(initial_infants-canceled_infants-noshow_infants)');

            $table->decimal('amount', 8, 2)->storedAs('(units*unit_price)+(adults*adult_price)+(childs*child_price)+(infants*infant_price)');
            $table->char('currency_id', 3)->default('USD');
            $table->decimal('unit_price', 8, 2)->default(0.00);
            $table->decimal('adult_price', 8, 2)->default(0.00);
            $table->decimal('child_price', 8, 2)->default(0.00);
            $table->decimal('infant_price', 8, 2)->default(0.00);

            $table->decimal('total_paid', 8, 2)->storedAs('(paid_cash+paid_credit)');

            $table->decimal('paid_cash', 8, 2)->default(0.00);
            $table->decimal('paid_credit', 8, 2)->default(0.00);
            $table->decimal('units_paid', 8, 2)->default(0.00);
            $table->decimal('adults_paid', 8, 2)->default(0.00);
            $table->decimal('childs_paid', 8, 2)->default(0.00);
            $table->decimal('infants_paid', 8, 2)->default(0.00);
            $table->char('paid_currency_id', 3)->default('USD');

            $table->decimal('discount', 8, 2)->default(0.00)->comment('discount amount has paid_currency');
            $table->string('discount_reason', 250)->nullable();

            $table->enum('salegroup', ['normal', 'invoice', 'complimentary'])
                  ->default('normal')
                  ->comment('Invoice means that no money was collected from customer, but invoiced to his operator or agency; 
                             Complimentary means that no money was paid and the customer is company\'s guest or employee');

            $table->integer('job_id')->unsigned()->nullable();
            $table->integer('collecting_job_id')->unsigned()->nullable();
            $table->integer('uncollecting_job_id')->unsigned()->nullable();

            $table->boolean('void')->default(false);
            $table->string('void_reason', 250)->nullable();

            $table->smallInteger('cancel_reason_id')->nullable();
            $table->string('cancel_reason', 250)->nullable();

            $table->smallInteger('noshow_reason_id')->nullable();
            $table->string('noshow_reason', 250)->nullable();

            $table->boolean('checked')->default(false);
            $table->boolean('accounted')->default(false);
            $table->boolean('liquidated')->default(false);
            
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
