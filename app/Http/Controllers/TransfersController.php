<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Location;
use App\Direction;
use App\Vehicle;
use App\Currency;
use App\Reservation;
use Carbon\Carbon;
use GoogleMaps;
use Mail;
use Session;
use App\Mail\NewReservation;

class TransfersController extends Controller
{
    public function index()
    {
        if(!request('from') or !request('to')){
            return redirect()->to('/');
        }

        $hasDirections = Location::whereHas('directions')
                             ->orWhere(function($query){
                                return $query->whereHas('returnDirections');
                             })
                             ->with(['descendants' => function($query){
                                return $query->whereIn('type', [
                                    'destination',
                                    'subdestination',
                                    'hotel'
                                ]);
                             }])
                             ->get();

        $locations = collect([]);
        foreach($hasDirections as $location){
            if($location->type !== 'hotel'){
                $location['trans'] = $location->cyrr;
            }
            $locations->push($location);
            foreach($location->descendants as $l){
                $locations->push($l);
                $l['trans'] = $l->cyrr;
            }
        }

        $currencies = Currency::get()->keyBy('id');

        $fromLocation = $locations->where('slug', request('from'))->first();
        $toLocation = $locations->where('slug', request('to'))->first();

        $distance = $this->getDistance($fromLocation->name, $toLocation->name);

        if($fromLocation->type == 'hotel'){
            $startLocation = $fromLocation->parent;
        }else{
            $startLocation = $fromLocation;
        }
        if($toLocation->type == 'hotel'){
            $endLocation = $toLocation->parent;
        }else{
            $endLocation = $toLocation;
        }

        $directions = Direction::where('start_location_id', $startLocation->id)
                               ->where('end_location_id', $endLocation->id)
                               ->first();
        if(!$directions){
            $directions = Direction::where('start_location_id', $endLocation->id)
                                   ->where('end_location_id', $startLocation->id)
                                   ->first();
        }

        $transfers = [];
        $indexing = true;
        $url = null;
        if($directions){
            $vehicles = Vehicle::with('attachments')->get();
            foreach($directions->prices as $price){
                $url = route('book', ['from' => $fromLocation->slug, 'to' => $toLocation->slug, 'vehicle' => $vehicles->where('id', $price['vehicle_id'])->first()->slug ]);
                if(array_key_exists('price', $price) && $price['price'] > 0){
                    $transfers[] = [
                        'price'   => $price,
                        'vehicle' => $vehicles->where('id', $price['vehicle_id'])->first(),
                        'url'     => $url
                    ];
                }
            }
        }

        if(!count($transfers)){
            $indexing = false;
        }
        //echo '<pre>'. print_r($locations, 1) . '</pre>';
        $otherLocations = Direction::with('end_location')->where('start_location_id', $fromLocation->id)->take(15)->get();
        return view('transfers', compact('indexing', 'otherLocations','locations', 'transfers', 'currencies', 'fromLocation', 'toLocation', 'distance'));
    }
    public function book(Request $request)
    {
        $locations = Location::get()->keyBy('slug');
        $currencies = Currency::get()->keyBy('id');

        $fromLocation = $locations->where('slug', request('from'))->first();
        $toLocation = $locations->where('slug', request('to'))->first();

        $distance = $this->getDistance($fromLocation->name, $toLocation->name);

        if($fromLocation->type == 'hotel'){
            $startLocation = $fromLocation->parent;
        }else{
            $startLocation = $fromLocation;
        }
        if($toLocation->type == 'hotel'){
            $endLocation = $toLocation->parent;
        }else{
            $endLocation = $toLocation;
        }

        $directions = Direction::where('start_location_id', $startLocation->id)
                               ->where('end_location_id', $endLocation->id)
                               ->first();
        if(!$directions){
            $directions = Direction::where('start_location_id', $endLocation->id)
                                   ->where('end_location_id', $startLocation->id)
                                   ->first();
        }

        $transfers = [];
        if($directions){
            $vehicles = Vehicle::with('attachments')->get();
            foreach($directions->prices as $price){
                if(array_key_exists('price', $price) && $price['price'] > 0){
                    $transfers[$price['vehicle_id']] = [
                        'price'     => $price,
                        'vehicle'   => $vehicles->where('id', $price['vehicle_id'])->first()
                    ];
                }
            }
            $selectedVehicle = $vehicles->where('slug', request('vehicle'))->first();
        }
        $child_seat_price = ['price' => 5, 'currency' => 'USD'];

        $services = $request->input('services', []);
        return view('book', compact('locations', 'transfers', 'currencies', 'fromLocation', 'toLocation', 'vehicles', 'selectedVehicle', 'child_seat_price', 'distance', 'services'));
    }
    public function makeBook(Request $request){
        $fromLocation = Location::find(request('pickup_location_id'));
        $toLocation = Location::find(request('dropoff_location_id'));

        if($fromLocation->type == 'hotel'){
            $startLocation = $fromLocation->parent;
        }else{
            $startLocation = $fromLocation;
        }
        if($toLocation->type == 'hotel'){
            $endLocation = $toLocation->parent;
        }else{
            $endLocation = $toLocation;
        }

        $direction = Direction::where('start_location_id', $startLocation->id)
                              ->where('end_location_id', $endLocation->id)
                              ->first();
        if(!$direction){
            $direction = Direction::where('start_location_id', $endLocation->id)
                                  ->where('end_location_id', $startLocation->id)
                                  ->first();
        }

        $price = collect($direction->prices)
            ->where('vehicle_id', request('vehicle_id'))
            ->first();

        $total = $price['price'];
        if(request('return_transfer')){
            $total *= 2;
        }
        // TODO: Store child seat prices in DB
        $total += (request('child_seats', 0) * 5);
        $total += (request('boosters', 0) * 5);

        // TODO: Additional services like flowers and champagne

        // TODO Get rates

        $rez = new Reservation;
        $rez->pickup_location_id = $request->input('pickup_location_id');
        $rez->dropoff_location_id = $request->input('dropoff_location_id');
        $rez->pickup_at = date('Y-m-d H:i:s', strtotime($request->input('flight_date') . ' ' . $request->input('flight_time', '00:00')));
        $rez->flight = $request->input('flight');
        $rez->flight_at = date('Y-m-d H:i:s', strtotime($request->input('flight_date') . ' ' . $request->input('flight_time', '00:00')));

        if(request('return_flight')){
            $rez->return_flight = request('return_flight');
        }
        if(request('return_flight_date')){
            $rez->return_pickup_at = date('Y-m-d H:i:s', strtotime(request('return_flight_date') . ' ' . request('return_flight_time', '00:00')));
        }
        if(request('return_flight_date')){
            $rez->return_flight_at = date('Y-m-d H:i:s', strtotime(request('return_flight_date') . ' ' . request('return_flight_time', '00:00')));
        }
        $rez->adults = (int)$request->input('adults', 1);
        $rez->children = (int)$request->input('children', 0);
        $rez->baggages = (int)$request->input('adults', 0);
        $rez->child_seats = (int)$request->input('child_seats', 0);
        $rez->boosters = (int)$request->input('boosters', 0);
        $rez->total = $total;
        $rez->currency = $price['currency'];
        $rez->payable = request('payment_amount', 0);
        $rez->payment_currency = $request->input('payment_currency', $request->input('currency', 'USD'));
        //$rez->services = $request->filled('services') ? explode(',', $request->input('services', '')) : null;
        $rez->status = 'pending';
        $rez->client_id = null;
        $rez->client_name = $request->input('name');
        $rez->client_phone = $request->input('telephone');
        $rez->client_email = $request->input('email');
        $rez->client_comment = $request->input('comments');
        $rez->vehicle_id = $request->input('vehicle_id');
        $rez->distance = (int)$request->input('distance', 0);
        $rez->duration = (int)$request->input('duration', 0);
        $rez->save();

        if($rez){

            // INCR direction popularity
            $direction->popularity = $direction->popularity + 1;
            $direction->save();

            // GET OR CREATE CUSTOMER

            $user = User::where('email', request('email'))
                ->orWhere(function($query){
                    $query->where('details->phone', request('telephone'))
                        ->orWhere('name', request('name'));
                })
                ->first();

            if(!$user){
                $user                   = new User;
                $user->name             = request('name');
                $user->email            = request('email');
                $user->details          = ['phone' => request('telephone')];
                $user->password         = bcrypt(time());
                $user->status           = 'new';
                $user->save();
                if($user){
                    $user->assignRole('customer');
                }
            }

            $rez->client_id = $user->id;
            $rez->save();

            Mail::to('transfer@fadotravel.com')
                ->cc('book@adaliatransfer.com')
                ->bcc('bayram4ik@gmail.com')
                ->send(new NewReservation($rez));
            return back()->withInput()
                         ->with('success', true)
                         ->with('message', trans('frontend.success-book'));
        }
    }

    public function payment(){
        //Gerçek ortam adresi PostUrl="https://sanalpos.ziraatbank.com.tr/v4/v3/Vposreq.aspx"
        //Test ortam adresi   PostUrl="http://sanalpos.innova.com.tr/ZIRAATBANK_v4/VposWeb/v3/Vposreq.aspx"

        $PostUrl      = 'http://sanalpos.innova.com.tr/ZIRAATBANK_v4/VposWeb/v3/Vposreq.aspx';
        $IsyeriNo     = '000000000495760';
        $IsyeriSifre  = null;//'!n-IG4_Zr';
        $KartNo       = $_POST["KartNo"];
        $KartAy       = $_POST["KartAy"];
        $KartYil      = $_POST["KartYil"];
        $KartCvv      = $_POST["KartCvv"];
        $Tutar        = 1;
        $SiparID      = '12412';
        $IslemTipi    = 'Sale';
        $TutarKodu    = '949';
        $ClientIp     = "212.2.199.55";

        $PosXML = 'prmstr=<VposRequest><MerchantId>'.$IsyeriNo.'</MerchantId><Password>'.$IsyeriSifre.'</Password><TransactionType>'.$IslemTipi.'</TransactionType><TransactionId>'.$SiparID.'</TransactionId><CurrencyAmount>'.$Tutar.'</CurrencyAmount><CurrencyCode>'.$TutarKodu.'</CurrencyCode><Pan>'.$KartNo.'</Pan><Cvv>'.$KartCvv.'</Cvv><Expiry>'.$KartYil.$KartAy.'</Expiry><TransactionDeviceSource>0</TransactionDeviceSource><ClientIp>'.$ClientIp.'</ClientIp></VposRequest>';

        echo '<h1>Vpos Request</h1>';
        echo $PostUrl."<br>";
        echo '<textarea rows="15" cols="60">'.$PosXML.'</textarea>';
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$PostUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$PosXML);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 59);
        //curl_setopt($ch, curl.options,array("CURLOPT_SSLVERSION"=>"CURL_SSLVERSION_DEFAULT"));
        //curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt ($ch, CURLOPT_CAINFO, "/var/www/localhost/certs/pos/cacert.pem");
        //İhtiyaç olması halinde aşağıdaki proxy açabilirsiniz.
        /*$proxy = "iproxy:8080";
        $proxy = explode(':', $proxy);
        curl_setopt($ch, CURLOPT_PROXY, $proxy[0]);
        curl_setopt($ch, CURLOPT_PROXYPORT, $proxy[1]);*/

        $result = curl_exec($ch);

        // Check for errors and display the error message
        if($errno = curl_errno($ch)) {
            //$error_message = curl_strerror($errno);
            echo "cURL error ({$errno}):\n";// {$error_message}";
        }
        curl_close($ch);

        echo '<h1>Vpos Response</h1>';
        echo '<textarea rows="15" cols="60">'.$result.'</textarea>';

    }

    public function redirectToTransfers(){
        session(['from' => request('from'), 'to' => request('to')]);
        return redirect()->route('transfers', ['from' => request('from'), 'to' => request('to')]);
    }

    private function getDistance($from, $to){
        $res = GoogleMaps::load('distancematrix')
            ->setParam([
                'origins'          => $from,
                'destinations'     => $to,
                'mode'             => 'driving'
            ])->get();
        return $res;
    }
}
