<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\FaqCategory;
use App\Reservation;
use App\Direction;
use App\Location;
use App\Vehicle;
use App\User;
use App\Http\Requests\ContactRequest;
use App\Mail\NewPartner;
use App\Mail\NewContactFormMessage;
use Mail;
class AboutController extends Controller{
    public function howitworks(){
        return view('howitworks');
    }
    public function fleet(){
        $vehicles = Vehicle::with('attachments')->orderBy('pax_capacity')->take(12)->get();
        $hasDirections = Location::whereHas('directions')
                             ->orWhere(function($query){
                                return $query->whereHas('returnDirections');
                             })
                             ->with(['descendants' => function($query){
                                return $query->whereIn('type', [
                                    'destination',
                                    'subdestination',
                                    'hotel'
                                ]);
                             }])
                             ->get();

        $locations = collect([]);
        foreach($hasDirections as $location){
            if($location->type !== 'hotel'){
                $location['trans'] = $location->cyrr;
            }
            $locations->push($location);
            foreach($location->descendants as $l){
                $locations->push($l);
                $l['trans'] = $l->cyrr;
            }
        }


        return view('fleet', compact('locations', 'vehicles'));
    }
    public function services(Request $request, $type = false){
        $locations = \App\Direction::with('start_location', 'end_location')
                                   ->whereHas('start_location', function($query){
                                        return $query->where('slug', 'antalya-airport');
                                   })
                                   ->orderBy(\DB::raw('RAND()'))
                                   ->take(20)
                                   ->get();
        if($type){
            $service = Service::where('slug', $type)
                              ->with('attachments')
                              ->firstOrFail();
            return view('service', compact('service', 'locations'));
        }else{
            $services = Service::with('attachments')->get();
            return view('services', compact('services'));
        }
    }
    public function contacts(){
        return view('contacts');
    }
    public function sendMessage(Request $request){
        if(!$request->filled('cap')){
            Mail::to('transfer@fadotravel.com')
                ->cc('book@adaliatransfer.com')
                ->bcc('bayram4ik@gmail.com')
                ->send(new NewContactFormMessage($request->input()));
            return back()->withInput()
                         ->with('success', true)
                         ->with('message', trans('frontend.success-message'));
        }
    }
    public function faq(){
        $faq_categories = FaqCategory::with('faqs')->get();
        return view('faq', compact('faq_categories'));
    }
    public function terms(){
        return view('terms');
    }
    public function partner(){
        $stats = [
            'partners'  => 0,
            'orders'    => 0,
            'customers' => 0,
            'routes'    => 0
        ];

        $stats['partners']  = User::role('agency')->count()+30;
        $stats['orders']    = Reservation::count()+378;
        $stats['customers'] = User::role('customer')->count()+179;
        $stats['routes']    = Direction::count()+1718;

        return view('partner', compact('stats'));
    }
    public function registerPartner(Request $request){
        $rules = [
            'name'           => 'required',
            'agency_address' => 'required',
            'agency_name'    => 'required',
            'agency_phone'   => 'required',
            'email'          => 'required',
        ];
        $data = $request->validate($rules);
        $partner = new User;
        $partner->name = $data['name'];
        $partner->email = $data['email'];
        $partner->details = [
            'agency_address' => $data['agency_address'],
            'agency_name'    => $data['agency_name'],
            'agency_phone'   => $data['agency_phone'],
        ];
        $partner->status = 'new';
        $partner->password = bcrypt($data['agency_name']);
        $partner->save();
        $partner->assignRole('agency');
        if($partner){
            Mail::to('transfer@fadotravel.com')
                ->cc('book@adaliatransfer.com')
                ->bcc('bayram4ik@gmail.com')
                ->send(new NewPartner($partner));
            return back()->withInput()
                         ->with('success', true)
                         ->with('message', trans('frontend.success-message'));
        }
    }
}
