<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Bnb\Laravel\Attachments\HasAttachment;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Service extends Model{
  use HasSlug, HasAttachment, HasTranslations;
  
  public $translatable = ['name', 'description', 'content'];
  public $guarded = [];

  public function toArray(){
    $attributes = parent::toArray();
    foreach($this->getTranslatableAttributes() as $name){
      $attributes[$name] = $this->getTranslation($name, app()->getLocale());
    }
    return $attributes;
  }
  public function getSlugOptions() : SlugOptions{
    return SlugOptions::create()
      ->generateSlugsFrom(['name'])
      ->saveSlugsTo('slug')
      ->usingLanguage('en')
      ->slugsShouldBeNoLongerThan(100);
  }
}
