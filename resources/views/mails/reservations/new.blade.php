<h2>Yeni rezervasyon oluşturuldu</h2>

<div>
Rez id: {{ $reservation->id }}<br>

Pickup Location: {{ $reservation->pickup_location->name }}<br>
Flight: {{ $reservation->flight }}<br>
Flight time: {{ $reservation->flight_at }}<br>

Dropoff Location: {{ $reservation->dropoff_location->name }}<br>
Pickup time: {{ $reservation->pickup_at }}<br>
Vehicle type: {{ $reservation->vehicle->name }} ({{ $reservation->vehicle->pax_capacity }} pax)<br>

Adults: {{ $reservation->adults }}<br>
Children: {{ $reservation->children }}<br>
Baggages: {{ $reservation->baggages }}<br>
Child seats: {{ $reservation->child_seats }}<br>
Boosters: {{ $reservation->boosters }}<br>
Total: {{ ($reservation->return_flight || $reservation->return_pickup_at)
            ? ($reservation->total/2) . ' ' . $reservation->currency
            : $reservation->total . ' ' . $reservation->currency
        }}
        <br>
Client name: {{ $reservation->client_name }}<br>
Client phone: {{ $reservation->client_phone }}<br>
Client e-mail: {{ $reservation->client_email }}<br>
Client comments: {{ $reservation->client_comment }}<br>
<br>



@if($reservation->return_flight_at)
<hr>
Return transfer date: {{ $reservation->return_pickup_at }}<br>
Pickup Location: {{ $reservation->dropoff_location->name }}<br>
Return flight: {{ $reservation->return_flight ?? 'Belirtilmemiş' }}<br>
Dropoff Location: {{ $reservation->pickup_location->name }}<br>
Vehicle type: {{ $reservation->vehicle->name }} ({{ $reservation->vehicle->pax_capacity }} pax)<br>
Adults: {{ $reservation->adults }}<br>
Children: {{ $reservation->children }}<br>
Baggages: {{ $reservation->baggages }}<br>
Child seats: {{ $reservation->child_seats }}<br>
Boosters: {{ $reservation->boosters }}<br>
{{ ($reservation->return_flight || $reservation->return_pickup_at)
            ? ($reservation->total/2) . ' ' . $reservation->currency
            : $reservation->total . ' ' . $reservation->currency
        }}
@endif

<hr>
Rez Date: {{ $reservation->created_at->format('d F Y H:i') }}

</div>