<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\User;
use App\Job;
use Carbon\Carbon;
use Cache;
use App\Http\Resources\UserResource;
use Hash;
use App\Events\EntryCreated;
use App\Events\EntryUpdated;

class UsersController extends Controller
{
    public $rules = [
        'name'              => 'required',
        'email'             => 'required|email',
        'phone'             => '',
        'status'            => ''
    ];
    public function index(Request $request)
    {
		return UserResource::collection(User::with('attachments')->role('admin')->get());
    }

    public function store(Request $request)
    {
        $user = $request->validate($this->rules);
        $user['password'] = Hash::make($request->input('password'));
        $user = User::create($user);
        $user->assignRole('admin');
        /*         
        if($request->input('can_pay_partial') === true){
            $user->givePermissionTo('pay-partial');
        } 
        */
        $user = new UserResource(User::with('attachments')->find($user->id));
        broadcast(new EntryCreated('users', $user->resolve()));
        return $user;
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $data = $request->validate($this->rules);
        if($request->has('passowrd')){
            $data['password'] = Hash::make($request->input('password'));
        }
        $user->update($data);
        /*
        if($request->input('can_pay_partial') === true){
            $user->givePermissionTo('pay-partial');
        }else{
            $user->revokePermissionTo('pay-partial');
        }
        */
        $user = new UserResource(User::with('attachments')->find($user->id));
        broadcast(new EntryUpdated('users', $user->resolve()));
        return $user;
    }

    public function destroy(User $user)
    {

    }

    public function upload(Request $request){
        $user = User::find($request->input('user_id'));
        $user->attach($request->file('file'));
        $user = new UserResource(User::with('attachments')->find($request->input('user_id')));
        broadcast(new EntryUpdated('users', $user->resolve()));
    }

    public function deleteimage(Request $request){
        User::find($request->input('user_id'))
            ->attachment($request->input('key'))
            ->delete();
        $user = new UserResource(User::with('attachments')->find($request->input('user_id')));
        broadcast(new EntryUpdated('users', $user->resolve()));
    }
}
