@extends('layouts.main')
@section('title', trans('frontend.home-title'))
@section('metadescr', trans('frontend.home-metadescr'))
@push('scripts')
<script>
	var app = new Vue({
		el: '#app',
		mixins: [currencyMixin]
	});
</script>
@endpush
@section('content')
	<div id="homepage-block-2" class="bgMove" style="height: 580px;background-image: url(assets/images/bg.jpg);">
		<div class="container">
			<h2>{{ trans('frontend.book-transfer-online') }}</h2>

			<form method="post" action="{{ route('redirectToTransfers') }}" class="main-search">
				{{ csrf_field() }}
				<div class="row" style="padding: 20px 0;">
					<div class="col-md-4 col-md-offset-2">
						<div class="form-group">
							<select class="form-control" style="height: 40px !important;" name="from" id="from" v-selectize placeholder="{{ trans('frontend.from-dots') }}">
								<option>&nbsp;</option>
								<optgroup label="{{ trans('frontend.airports') }}">
									@foreach($locations->where('type', 'airport') as $location)
									<option  value="{{ $location->slug }}" @if(request('from', session('from')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
								<optgroup label="{{ trans('frontend.destinations') }}">
									@foreach($locations->where('type', 'destination') as $location)
									<option  value="{{ $location->slug }}" @if(request('from', session('from')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
								<optgroup label="{{ trans('frontend.locations') }}">
									@foreach($locations->where('type', 'subdestination') as $location)
									<option  value="{{ $location->slug }}" @if(request('from', session('from')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
								<optgroup label="{{ trans('frontend.hotels') }}">
									@foreach($locations->where('type', 'hotel')->sortBy('name') as $location)
									<option  value="{{ $location->slug }}" @if(request('from', session('from')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<select v-selectize class="form-control" style="height: 40px !important;" name="to" id="to" placeholder="{{ trans('frontend.to-dots') }}">
								<option>&nbsp;</option>
								<optgroup label="{{ trans('frontend.airports') }}">
									@foreach($locations->where('type', 'airport') as $location)
									<option  value="{{ $location->slug }}" @if(request('to', session('to')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
								<optgroup label="{{ trans('frontend.destinations') }}">
									@foreach($locations->where('type', 'destination') as $location)
									<option  value="{{ $location->slug }}" @if(request('to', session('to')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
								<optgroup label="{{ trans('frontend.locations') }}">
									@foreach($locations->where('type', 'subdestination') as $location)
									<option  value="{{ $location->slug }}" @if(request('to', session('to')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
								<optgroup label="{{ trans('frontend.hotels') }}">
									@foreach($locations->where('type', 'hotel')->sortBy('name') as $location)
									<option  value="{{ $location->slug }}" @if(request('to', session('to')) == $location->slug) selected @endif>
										{{ $location->name }}
									</option>
									@endforeach
								</optgroup>
							</select>
						</div>
					</div>
				</div>
				<input type="submit" class="btn btn-yellow btn-bg-dark btn-lg" value="{{ trans('frontend.find-transfer') }}" style="margin: 0 10px; padding: 12px 60px;">
			</form>
			<div id="large-image">
				<img src="/assets/images/cars-3204.png" alt="Taxi" >
				<div class="dialog" style="width: 220px;top: -58px;">
					<span class="fa fa-whatsapp"></span>
					<h4 class="white">{{ trans('frontend.call-us-now') }}</h4>
					<h3 class="yellow">{{ $settings->company_phone }}</h3>
				</div>
			</div>
			<div class="dialog hidden-md hidden-lg" style="margin-top: -40px;">
					<h4 class="white"><span class="fa fa-whatsapp" style="color: #FFC61A;"></span> &nbsp;{{ trans('frontend.call-us-now') }}</h4>
					<h3 class="yellow">{{ $settings->company_phone }}</h3>
				</div>
		</div>
	</div>
	<div class="homepage-block-yellow-2" style="height: 120px;">
	</div>

	<section id="services">
		<div class="container">
			<h4 class="yellow">{{ trans('frontend.welcome') }}</h4>
			<h2 class="h1">{{ trans('frontend.how-it-works') }}</h2>
			<div class="row">
				<div class="col-md-3 col-sm-6 col-ms-6 matchHeight">
					<div class="image">
						<img src="assets/images/manual-1.png" alt="Service">
					</div>
					<h5>{{ trans('frontend.select-transfer') }}</h5>
					<p>{{ trans('frontend.select-route') }}</p>
				</div>
				<div class="col-md-3 col-sm-6 col-ms-6 matchHeight">
					<div class="image">
						<img src="assets/images/manual-2.png" alt="Service">
					</div>
					<h5>{{ trans('frontend.make-booking') }}</h5>
					<p>{{ trans('frontend.fill-form') }}</p>
				</div>
				<div class="col-md-3 col-sm-6 col-ms-6 matchHeight">
					<div class="image">
						<img src="assets/images/manual-3.png" alt="Service">
					</div>
					<h5>{{ trans('frontend.find-driver') }}</h5>
					<p>{{ trans('frontend.find-driver-desc') }}</p>
				</div>
				<div class="col-md-3 col-sm-6 col-ms-6 matchHeight">
					<div class="image">
						<img src="assets/images/manual-4.png" alt="Service">
					</div>
					<h5>{{ trans('frontend.fast-affordable') }}</h5>
					<p>{{ trans('frontend.enjoy-holidays') }}</p>
				</div>
			</div>
		</div>
	</section>
<section id="tariffs">
		<div class="container">
			<h4 class="yellow">{{ trans('frontend.see-our') }}</h4>
			<h2 class="h1">{{ trans('frontend.best-tariffs') }}</h2>
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="item matchHeight">
						<div class="image">
							<a href="{{ route('fleet') }}">
								<img src="assets/images/standart.png" class="full-width">
							</a>
						</div>
						<h4>
							<a href="{{ route('fleet') }}" style="color: #222;">
								{{ trans('frontend.standard') }}
							</a>
						</h4>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="item matchHeight">
						<div class="image">
							<a href="{{ route('fleet') }}">
								<img src="assets/images/minivan.png" class="full-width">
							</a>
						</div>
						<h4><a href="{{ route('fleet') }}" style="color: #222;">{{ trans('frontend.minivan') }}</a></h4>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="item matchHeight vip">
						<div class="image">
							<a href="{{ route('fleet') }}">
								<img src="assets/images/vip-mini.png" class="full-width">
							</a>
						</div>
						<h4 class="red">
							<a href="{{ route('fleet') }}" class="red">{{ trans('frontend.vip-minivan') }}</a></h4>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="item matchHeight">
						<div class="image">
							<a href="{{ route('fleet') }}">
								<img src="assets/images/minibus.png" class="full-width">
							</a>
						</div>
						<h4><a href="{{ route('fleet') }}" style="color: #222;">{{ trans('frontend.minibus') }}</a></h4>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="download" class="parallax" style="background-image: url(assets/images/download-bg.jpg);">
		<div class="container">
			<h4 class="yellow">{{ trans('frontend.exclusive-services') }}</h4>
			<h2 class="h1">{{ trans('frontend.travel-without-limits') }}</h2>
			<div class="row">
				<div class="col-md-4 col-sm-12">
					<div class="items row">
						<div class="col-md-2 visible-md visible-lg">
							<span class="num">01.</span>
						</div>
						<div class="col-md-10">
							<h5 class="yellow">
								<a href="{{ route('services') }}">
									{{ $services['antalya-cip-terminal']['name'] }}
								</a>
							</h5>
							<p>{{ $services['antalya-cip-terminal']['description'] }}</p>
						</div>
						<div class="col-md-2 visible-md visible-lg">
							<span class="num">02.</span>
						</div>
						<div class="col-md-10">
							<h5 class="yellow">
								<a href="{{ route('services') }}">
									{{ $services['antalya-fast-direct']['name'] }}
								</a>
							</h5>
							<p>{{ $services['antalya-fast-direct']['description'] }}</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-md-push-4 col-sm-12">
					<div class="items items-right row">
						<div class="col-md-10">
							<h5 class="yellow">
								<a href="{{ route('services') }}">
									{{ $services['champagne-meeting']['name'] }}
								</a>
							</h5>
							<p>{{ $services['champagne-meeting']['description'] }}</p>
						</div>
						<div class="col-md-2 visible-md visible-lg">
							<span class="num">03.</span>
						</div>
						<div class="col-md-10">
							<h5 class="yellow">
								<a href="{{ route('services') }}">
									{{ $services['flowers-meeting']['name'] }}
								</a>
							</h5>
							<p>{{ $services['flowers-meeting']['description'] }}</p>
						</div>
						<div class="col-md-2 visible-md visible-lg">
							<span class="num">04.</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="car-block">
		<div class="car-right animation-block">
			<img src="assets/images/_car-big-side3.png" alt="Car">
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<h4 class="yellow">{{ trans('frontend.for-travel-agencies') }}</h4>
					<h2 class="h1">{{ trans('frontend.do-you-want-earn') }}</h2>
				</div>
				<div class="col-md-6">
					{!! trans('frontend.for-travel-agencies-features') !!}
					<a href="{{ route('partner') }}" class="btn btn-yellow btn-lg btn-white">{{ trans('frontend.become-partner') }}</a>
				</div>
			</div>
		</div>
	</section>
	<section id="testimonials">
		<hr class="lg">
		<div class="container">
			<h2 class="yellow">{{ trans('frontend.happy-customers') }}</h2>

			<div class="swiper-container row" id="testimonials-slider">
				<div class="swiper-wrapper">
					<div class="col-md-4 col-sm-6 swiper-slide">
						<div class="inner matchHeight">
							<div class="text">
								<p>It was great that the driver was in the airport even my fligth was delayed nearly two hours.<br>Thank you.</p>
							</div>
							<div class="quote">
								<span class="fa fa-quote-left"></span>
								<div class="name">George King</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 swiper-slide">
						<div class="inner matchHeight">
							<div class="text">
								<p>The driver was easy to find &amp; very helpful, the car &amp; facilities were as described, the transfer was quicker than, very good service &amp; I would recommend.</p>
							</div>
							<div class="quote">
								<span class="fa fa-quote-left"></span>
								<div class="name">David Calley</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 swiper-slide">
						<div class="inner matchHeight">
							<div class="text">
								<p>Всё было организованно на отлично. Несмотря на задержку рейса, нас ожидал водитель. Машина была на класс выше (без доплаты). Мы очень довольны сервисом.</p>
							</div>
							<div class="quote">
								<span class="fa fa-quote-left"></span>
								<div class="name">Kristina Bradu</div>
							</div>
						</div>
					</div>
				</div>
				<div class="arrows">
					<a href="#" class="arrow-left fa fa-caret-left"></a>
					<a href="#" class="arrow-right fa fa-caret-right"></a>
				</div>
			</div>
		</div>
	</section>
	<div id="homepage-banners">
		<div class="container">
			<div class="row">
				<a href="#" class="col-md-6 col-sm-6 col-ms-6">
					<img src="assets/images/banners/be-our-clients.jpg" class="full-width" alt="Banner">
				</a>
				<a href="#" class="col-md-6 col-sm-6 col-ms-6">
					<img src="assets/images/banners/ontime-everytime.jpg" class="full-width" alt="Banner">
				</a>
			</div>
		</div>
	</div>

	<section id="partners">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<h4 class="black margin-0">{{ trans('frontend.latest') }}</h4>
					<h2 class="white margin-0">{{ trans('frontend.from-blog') }}</h2>
				</div>
				<div class="col-md-9 col-sm-12">
					<div class="row items">
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection