<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Location;
use Carbon\Carbon;

class Slugify extends Command
{

    protected $signature = 'slugify';

    protected $description = 'Generates slugs';

    public function handle()
    {
        $locations = Location::get();
        $now = Carbon::now();
        foreach($locations as $location){
            $location->created_at = $now;
            $location->save();
        }
    }
}
