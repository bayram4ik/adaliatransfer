<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Kalnoy\Nestedset\NestedSet;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->char('code', 20)->nullable();
            $table->enum('type', ['destination','subdestination','district','city','country','place','hotel','meetingpoint', 'airport', 'terminal']);
            $table->jsonb('details')->nullable();
            $table->decimal('lat', 11, 8)->default(0.00);
            $table->decimal('lon', 11, 8)->default(0.00);
            NestedSet::columns($table);
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
