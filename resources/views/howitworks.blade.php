@extends('layouts.main')
@section('title', trans('frontend.how-it-works'))
@push('scripts')
<script>
    var app = new Vue({
        el: '#app',
        mixins: [currencyMixin]
    });
</script>
@endpush
@section('content')
    <header class="page-header bgMove" style="background-image: url(assets/images/_inner-bg.jpg);">
        <div class="container">
            <h1>{{ trans('frontend.how-it-works') }}</h1>
        </div>
    </header>
    <div class="container">
        <div class="inner inner-two-col row">
            <div class="col-lg-8 col-lg-offset-2 text-page">
                <p>{!! trans('frontend.how-it-works-desc') !!}</p>
                <div class="row" style="margin-top:40px;">
                    <div class="col-md-3">
                        <img src="{{ asset('images/findroute.png') }}" width="160">
                    </div>
                    <div class="col-md-9">
                        <h4 style="margin-top:0px;">{{ trans('frontend.find-route') }}</h4>
                        <blockquote>
                            {{ trans('frontend.find-route-desc') }}
                        </blockquote>
                    </div>
                </div>
                <div class="row" style="margin-top:40px;">
                    <div class="col-md-9">
                        <h4 style="margin-top:0px;">
                            {{ trans('frontend.book-your-transfer-online') }}
                        </h4>
                        <blockquote style="border-left: 0;border-right: 4px solid #FFC61A;margin-left:0;padding-right: 30px; margin-right:30px;padding-left: 0px;">
                            {{ trans('frontend.book-your-transfer-online-desc') }}
                        </blockquote>
                    </div>
                    <div class="col-md-3">
                        <img src="{{ asset('images/reservation.png') }}" width="160">
                    </div>
                </div>
                <div class="row" style="margin-top:40px;">
                    <div class="col-md-3">
                        <img src="{{ asset('images/driver.png') }}" width="160">
                    </div>
                    <div class="col-md-9">
                        <h4 style="margin-top:0px;">{{ trans('frontend.driver-meets-you') }}</h4>
                        <blockquote>
                            {{ trans('frontend.driver-meets-you-desc') }}
                        </blockquote>
                    </div>

                </div>
                <div class="row" style="margin-top:40px;">
                    <div class="col-md-12">
                        <h4 style="margin-top:0px;" class="aligncenter">{{ trans('frontend.have-nice-holidays') }}</h4>
                        <img  class="aligncenter" src="{{ asset('images/haveaniceholiday.png') }}" width="160">
                    </div>
                </div>

                <hr>
            </div>
        </div>
    </div>

@endsection