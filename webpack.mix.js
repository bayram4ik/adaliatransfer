let mix = require('laravel-mix');

mix.webpackConfig({
  module: {
    rules: [{
      test: /\.pug$/,
      oneOf: [{
        resourceQuery: /^\?vue/,
        use: ['pug-plain-loader']
      }]
    }]
  }
});

mix.scripts([
    'public/js/plugins/ui/moment/moment.min.js',
    //'public/js/plugins/pickers/daterangepicker.js',
    'public/js/plugins/forms/styling/switchery.min.js',
    //'public/js/plugins/forms/styling/uniform.min.js',
    //'public/js/plugins/forms/selects/bootstrap_multiselect.js',
    'public/js/core/libraries/bootstrap.min.js',
    //'public/js/plugins/ui/drilldown.js',
    'public/js/plugins/ui/nicescroll.min.js'
], 'public/js/plugins.js');

mix.copy('node_modules/jquery/dist/jquery.slim.min.js','public/js').version();

mix.scripts([
    'public/js/core/app.js'
], 'public/js/all.js');

mix.js('resources/assets/js/app.js', 'public/js')
   .extract([
    //'lodash',
    'axios',
    'vue',
    'vue-router',
    'vuex',
    'vuex-persistedstate',
    'collect.js',
    'moment',
    //'jquery'
]);

