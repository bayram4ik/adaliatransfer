<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id')->unsigned()->index();
            $table->integer('location_id')->unsigned()->index();
            $table->integer('company_id')->unsigned()->index();
            $table->string('description', 250)->nullable();
            $table->enum('type', ['buy', 'sell'])->default('buy');
            $table->decimal('unit_price', 8, 2)->default(0.00);
            $table->decimal('adult_price', 8, 2)->default(0.00);
            $table->decimal('child_price', 8, 2)->default(0.00);
            $table->decimal('infant_price', 8, 2)->default(0.00);
            $table->tinyInteger('chd_min_age')->unsigned()->default(0);
            $table->tinyInteger('chd_max_age')->unsigned()->default(0);
            $table->tinyInteger('inf_min_age')->unsigned()->default(0);
            $table->tinyInteger('inf_max_age')->unsigned()->default(0);
            $table->char('currency', 3);
            $table->timestampTz('starts_at')->nullable();
            $table->timestampTz('ends_at')->nullable();
            $table->boolean('active')->default(false);
            $table->timestampsTz();

            $table->index(['active', 'service_id', 'location_id', 'company_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_prices');
    }
}
