<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleOpeningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_openings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_variation_id')->unsigned();
            $table->date('date');
            $table->integer('seller_id');
            $table->integer('quota')->default(0)->comment('Either pax or unit quota');
            $table->timestampTz('valid_before')->nullable()->comment('Seller won\'t be able to sell after specified time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temporary_sale_openings');
    }
}
