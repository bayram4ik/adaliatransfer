<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/core.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/components.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/colors.css') }}" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="{{ asset('js/core/libraries/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/forms/styling/uniform.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/pages/login.js') }}"></script>
    </head>
    <body class="login-container bg-slate-800">
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    <form method="post" action="{{ route('password.email') }}">
                        {{ csrf_field() }}
                        <div class="panel panel-body login-form">
                            <div class="text-center">
                                <div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
                                <h5 class="content-group">Password recovery <small class="display-block">We'll send you instructions in email</small></h5>
                            </div>
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                <input name="email" type="email" class="form-control" placeholder="Your email" value="{{ old('email') }}" required>
                                <div class="form-control-feedback">
                                    <i class="icon-mail5 text-muted"></i>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <button type="submit" class="btn bg-blue btn-block">Reset password <i class="icon-spinner11 position-right"></i></button>

                            <div class="content-divider text-muted form-group mt-20"><span>Remembered password?</span></div>
                            <a href="{{ route('login') }}" class="btn bg-green btn-block btn-xs"><i class="icon-circle-left2 position-left"></i> Go to login page </a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
