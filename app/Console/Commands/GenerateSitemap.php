<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use App\Location;
use App\Direction;
use LaravelLocalization;
class GenerateSitemap extends Command
{
    protected $signature = 'generate:sitemap';
    protected $description = 'Generate website sitemap';
    public function __construct()
    {
        parent::__construct();
    }
    public function handle()
    {
        $hasDirections = Location::whereHas('directions')
                             ->orWhere(function($query){
                                return $query->whereHas('returnDirections');
                             })
                             ->with(['descendants' => function($query){
                                return $query->whereIn('type', [
                                    'destination',
                                    'subdestination',
                                    'hotel'
                                ]);
                             }])
                             ->get();
        $locations = collect([]);
        foreach($hasDirections as $location){
            $locations->push($location);
            foreach($location->descendants as $l){
                $locations->push($l);
            }
        }

        $sitemap = SitemapGenerator::create(route('home'))->getSitemap();

        foreach($locations->where('type', 'airport') as $airport){
            foreach($locations->where('type', 'hotel') as $hotel){

                $arrRu = LaravelLocalization::getLocalizedURL('ru', route('transfers', ['from' => $airport->slug, 'to' => $hotel->slug]));
                $arrEn = LaravelLocalization::getLocalizedURL('en', route('transfers', ['from' => $airport->slug, 'to' => $hotel->slug]));

                $depRu = LaravelLocalization::getLocalizedURL('ru', route('transfers', ['to' => $airport->slug, 'from' => $hotel->slug]));
                $depEn = LaravelLocalization::getLocalizedURL('en', route('transfers', ['to' => $airport->slug, 'from' => $hotel->slug]));

                $sitemap->add(Url::create($arrRu)->setPriority(0.5)->addAlternate($arrEn, 'en'));
                $sitemap->add(Url::create($depRu)->setPriority(0.5)->addAlternate($depEn, 'en'));
            }
        }
        $sitemap->writeToFile(public_path('sitemap.xml'));
    }
}