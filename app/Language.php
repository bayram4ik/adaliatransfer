<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Language extends Model{
    use HasTranslations;
    
    public $translatable = ['name'];
    public $incrementing = false;
    public $timestamps = false;
}
