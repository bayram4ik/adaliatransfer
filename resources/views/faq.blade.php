@extends('layouts.main')
@section('title', trans('frontend.faq'))
@push('scripts')
<script>
    var app = new Vue({
        el: '#app',
        mixins: [currencyMixin]
    });
</script>
@endpush
@section('content')
    <header class="page-header" style="background-image: url(assets/images/_inner-bg.jpg);">
        <div class="container">
            <ol class="bread">
                <li>
                    <a href="{{ route('home') }}"><span><i class="fa fa-home"></i></span></a>
                </li>
                <li class="divider"><span>//</span></li>
                <li>
                    <span>{{ trans('frontend.faq') }}</span>
                </li>
            </ol>
            <h1>{{ trans('frontend.faq') }}</h1>
        </div>
    </header>

	<div class="container">
        @foreach($faq_categories as $category)
        <h2 style="margin-top:22px;">{{ $category->name }}</h2>
        <div class="accordion">
            @foreach($category->faqs as $faq)
            <h4>{{ $faq->question }}</h4>
            <div>{!! $faq->answer !!}</div>
            @endforeach
        </div>
        @endforeach
    </div>
@endsection