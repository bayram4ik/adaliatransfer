<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Bnb\Laravel\Attachments\HasAttachment;
use Spatie\Translatable\HasTranslations;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Vehicle extends Model{
    use HasSlug, HasAttachment, HasTranslations;
    
    public $translatable = ['name', 'models'];
    public $timestamps = false;
    public $guarded = [];
    public function toArray(){
        $attributes = parent::toArray();
        foreach ($this->getTranslatableAttributes() as $name) {
            $attributes[$name] = $this->getTranslation($name, app()->getLocale());
        }
        return $attributes;
    }
    public function getSlugOptions() : SlugOptions{
        return SlugOptions::create()
            ->generateSlugsFrom(['name', 'pax_capacity', 'baggage_capacity'])
            ->saveSlugsTo('slug')
            ->usingLanguage('tr')
            ->slugsShouldBeNoLongerThan(100);
    }
}
