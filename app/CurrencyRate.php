<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrencyRate extends Model{
	
    public $casts = [
        'buying' => 'float',
        'selling' => 'float'
    ];
}
