<?php namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class LocationResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'code'          => $this->code,
            'type'          => $this->type,
            'details'       => $this->details ?? [],
            'lat'           => $this->lat,
            'lon'           => $this->lon,
            'parent_id'     => $this->parent_id,
            'parent'        => $this->parent ? new LocationResource($this->parent) : null,
            'parent_name'   => $this->parent ? $this->parent->name : null,
            'children'      => $this->whenLoaded('children'),
            'sort_order'    => $this->sort_order,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
            'deleted_at'    => $this->deleted_at   
        ];
    }
}
