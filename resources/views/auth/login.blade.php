<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/core.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/components.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/colors.css') }}" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="{{ asset('js/core/libraries/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugins/forms/styling/uniform.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/pages/login.js') }}"></script>
    </head>

    <body class="login-container bg-slate-800">
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    <form action="{{ route('login') }}" method="post">
                        {{ csrf_field() }}
                        <div class="panel panel-body login-form">
                            <div class="text-center">
                                <div class="icon-object border-warning-400 text-warning-400"><i class="icon-people"></i></div>
                                <h5 class="content-group-lg">Login to your account <small class="display-block">Enter your credentials</small></h5>
                            </div>

                            <div class="form-group has-feedback has-feedback-left {{ $errors->has('email') ? 'has-error' : '' }}">
                                <input name="email" type="email" class="form-control" placeholder="E-mail" required>
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                                @if($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group has-feedback has-feedback-left {{ $errors->has('password') ? 'has-error' : '' }}">
                                <input name="password" type="password" class="form-control" placeholder="Password" required>
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                                @if($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group login-options">
                                <label class="checkbox-inline">
                                    <input name="remember" type="checkbox" class="styled" {{ old('remember') ? 'checked' : '' }}>
                                    Remember
                                </label>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn bg-blue btn-block">
                                    Login <i class="icon-circle-right2 position-right"></i>
                                </button>
                            </div>

                            <div class="content-divider text-muted form-group mt-20"><span>Forgot password?</span></div>
                            <a href="{{ route('password.request') }}" class="btn btn-danger btn-block btn-xs">
                                Reset password <i class="icon-spinner11 position-right"></i>
                            </a>

                            
                            {{-- <span class="help-block text-center no-margin">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> and <a href="#">Cookie Policy</a></span> --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
