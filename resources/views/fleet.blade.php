@extends('layouts.main')
@section('title', trans('frontend.best-tariffs'))
@section('metadescr', trans('frontend.best-tariffs'))
@push('scripts')
<script>
    var app = new Vue({
        el: '#app',
        mixins: [currencyMixin]
    });
</script>
@endpush
@section('content')
    <header class="page-header">
        <div class="container">
            <ol class="bread">
                <li>
                    <a href="{{ route('home') }}"><span><i class="fa fa-home"></i></span></a>
                </li>
            </ol>
            <h1 style="font-size: 30px;">{{ trans('frontend.best-tariffs') }}</h1>
        </div>
    </header>

    <div class="container">

    <form method="post" action="{{ route('redirectToTransfers') }}">
                {{ csrf_field() }}
                <div class="row" style="padding: 0;">
                    <div class="col-md-4 col-md-offset-1">
                        <div class="form-group">
                            <select class="form-control" style="height: 40px !important;" name="from" id="from" v-selectize placeholder="{{ trans('frontend.from-dots') }}">
                                <option>&nbsp;</option>
                                <optgroup label="{{ trans('frontend.airports') }}">
                                    @foreach($locations->where('type', 'airport') as $location)
                                    <option data-data="{{ $location->toJson(JSON_HEX_APOS) }}" value="{{ $location->slug }}" @if(request('from', session('from')) == $location->slug) selected @endif>
                                        {{ $location->name }}
                                    </option>
                                    @endforeach
                                </optgroup>
                                <optgroup label="{{ trans('frontend.destinations') }}">
                                    @foreach($locations->where('type', 'destination') as $location)
                                    <option data-data="{{ $location->toJson(JSON_HEX_APOS) }}" value="{{ $location->slug }}" @if(request('from', session('from')) == $location->slug) selected @endif>
                                        {{ $location->name }}
                                    </option>
                                    @endforeach
                                </optgroup>
                                <optgroup label="{{ trans('frontend.locations') }}">
                                    @foreach($locations->where('type', 'subdestination') as $location)
                                    <option data-data="{{ $location->toJson(JSON_HEX_APOS) }}" value="{{ $location->slug }}" @if(request('from', session('from')) == $location->slug) selected @endif>
                                        {{ $location->name }}
                                    </option>
                                    @endforeach
                                </optgroup>
                                <optgroup label="{{ trans('frontend.hotels') }}">
                                    @foreach($locations->where('type', 'hotel')->sortBy('name') as $location)
                                    <option data-data="{{ $location->toJson(JSON_HEX_APOS) }}" value="{{ $location->slug }}" @if(request('from', session('from')) == $location->slug) selected @endif>
                                        {{ $location->name }}
                                    </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
<select v-selectize class="form-control" style="height: 40px !important;" name="to" id="to" placeholder="{{ trans('frontend.to-dots') }}">
                                <option>&nbsp;</option>
                                <optgroup label="{{ trans('frontend.airports') }}">
                                    @foreach($locations->where('type', 'airport') as $location)
                                    <option data-data="{{ $location->toJson(JSON_HEX_APOS) }}" value="{{ $location->slug }}" @if(request('to', session('to')) == $location->slug) selected @endif>
                                        {{ $location->name }}
                                    </option>
                                    @endforeach
                                </optgroup>
                                <optgroup label="{{ trans('frontend.destinations') }}">
                                    @foreach($locations->where('type', 'destination') as $location)
                                    <option data-data="{{ $location->toJson(JSON_HEX_APOS) }}" value="{{ $location->slug }}" @if(request('to', session('to')) == $location->slug) selected @endif>
                                        {{ $location->name }}
                                    </option>
                                    @endforeach
                                </optgroup>
                                <optgroup label="{{ trans('frontend.locations') }}">
                                    @foreach($locations->where('type', 'subdestination') as $location)
                                    <option data-data="{{ $location->toJson(JSON_HEX_APOS) }}" value="{{ $location->slug }}" @if(request('to', session('to')) == $location->slug) selected @endif>
                                        {{ $location->name }}
                                    </option>
                                    @endforeach
                                </optgroup>
                                <optgroup label="{{ trans('frontend.hotels') }}">
                                    @foreach($locations->where('type', 'hotel')->sortBy('name') as $location)
                                    <option data-data="{{ $location->toJson(JSON_HEX_APOS) }}" value="{{ $location->slug }}" @if(request('to', session('to')) == $location->slug) selected @endif>
                                        {{ $location->name }}
                                    </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1">
                    <button type="submit" class="btn btn-yellow btn-bg-dark btn-lg" style="margin: 0 10px; padding: 7px 60px;">
                        <i class="fa fa-search"></i>
                    </button>

                    </div>
                </div>
            </form>


        </div>
    <section id="tariffs">
        <div class="container">
            <div class="row">
                @foreach($vehicles as $vehicle)
                <div class="col-md-3 col-sm-6" style="padding-bottom: 25px;">
                    <div class="item matchHeight">
                        <div class="image">
                            <img src="{{ $vehicle->attachments[0]['url'] }}" class="full-width" alt="Tariff">
                        </div>
                        <h4 style="margin-bottom: 3px;">
                            {{ $vehicle->name }}
                        </h4>
                        <span style="font-size: 14px;font-weight: 600;color:#444" class="text-center">
                                {{ $vehicle->pax_capacity }} {{ trans('a.pax') }}
                        </span><br>
                        <small style="line-height: 1em; color: #333">{{ $vehicle->models }}</small>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </section>

    </div>
@endsection