<?php namespace App\Events;

//use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;

class EntryUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $path;
    public $entry;

    public function __construct($path, $entry){
        $this->path  = $path;
        $this->entry = $entry;
    }

    public function broadcastWith(){
        return [
            'path'  => $this->path,
            'entry' => $this->entry
        ];
    }

    public function broadcastOn(){
        return new PrivateChannel('updates');
    }
}
