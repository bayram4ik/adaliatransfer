<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourVariationPickuptimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_variation_pickuptimes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_variation_id')->unsigned();
            $table->date('starts_at');
            $table->date('ends_at');
            $table->jsonb('times')
                  ->comment('{hotel:time} or {hotel:markets:time} or {hotel:seances:time} or {hotel:markets:seances:time}');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_variation_pickuptimes');
    }
}
