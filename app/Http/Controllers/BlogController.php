<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(Request $request, $post = false)
    {
        if($post){
            return view('post');
        }
        return view('blog');
    }
}
