<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->char('id', 20);
            $table->integer('dep_location_id')->unsigned();
            $table->integer('arr_location_id')->unsigned();
            $table->timestamps();

            $table->primary('id');
            $table->unique(['id', 'dep_location_id', 'arr_location_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flights');
    }
}
