<?php namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class AgencyResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'name'                      => $this->name,
            'status'                    => $this->status,
            'email'                     => $this->email,
            'created_at'                => $this->created_at,
            'deleted_at'                => $this->deleted_at,
            'agency_name'               => $this->details ? $this->details['agency_name'] : null,
            'agency_comission'          => $this->details && array_key_exists('agency_comission', $this->details) ? $this->details['agency_comission'] : null,
            'agency_address'            => $this->details && array_key_exists('agency_address', $this->details) ? $this->details['agency_address'] : null,
            'phone'                     => $this->details && array_key_exists('phone', $this->details) ? $this->details['phone'] : null,
            'can_pay_partial'           => $this->hasPermissionTo('pay-partial') ? true : false,
            'pays_partial'              => $this->hasPermissionTo('pay-partial') ? 'Evet' : 'Hayir',
            'banned_at'                 => $this->banned_at
        ];
    }
}
