<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Bnb\Laravel\Attachments\HasAttachment;
use Spatie\Translatable\HasTranslations;

class Setting extends Model{

    use HasAttachment, HasTranslations;

    public $translatable = [
        'meta_description',
        'meta_keywords',
        'company_phone',
        'about_us_short'
    ];

    public $incrementing = false;

    public $casts = [
        'active_languages' => 'array',
        'social' => 'object',
        'indexing' => 'boolean'
    ];
}
