<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDutyCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('duty_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('duty_id')->unsigned();
            $table->integer('period_id')->unsigned();
            $table->decimal('mini', 8, 2)->default(0);
            $table->decimal('midi', 8, 2)->default(0);
            $table->decimal('bus', 8, 2)->default(0);
            $table->char('currency_code', 3)->default('TRY');
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('duty_costs');
    }
}
