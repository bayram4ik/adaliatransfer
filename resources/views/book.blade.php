@extends('layouts.main')
@section('title', trans('frontend.booking-trf-from') . ' '. $fromLocation->name . ' ' . trans('frontend.to') . ' ' . $toLocation->name)
@push('scripts')
<script>
	var app = new Vue({
		el: '#app',
		mixins: [currencyMixin],
		data: {
			locations: {!! json_encode($locations) !!},
			transfers: {!! json_encode($transfers) !!},
			vehicles: {!! json_encode($vehicles) !!},
			from: '{{ request('from') }}',
			to: '{{ request('to') }}',
			champagnePrice: 45,
			bouquetPrice: 20,
			services: {!! json_encode($services) !!},
			selectedVehicle: {!! json_encode($selectedVehicle) !!},
			child_seat_price: {!! json_encode($child_seat_price) !!},
			form: {},
			distance: {!! $distance !!}
		},
		computed: {
			cost: function(){
				return parseInt(this.transfers[this.selectedVehicle.id].price.price)+(parseInt(this.child_seat_price.price)*((parseInt(this.form.child_seats) || 0)+(parseInt(this.form.boosters) || 0)));
			},
			total: function(){
				let total = this.cost*(this.form.has_return_transfer ? 2 : 1);
				if(this.services.includes('champagne')){
					total += this.champagnePrice
				}
				if(this.services.includes('bouquet')){
					total += this.bouquetPrice
				}
				return total;
			},
			child_seats_count: function(){
				return (parseInt(this.form.child_seats) || 0)+(parseInt(this.form.boosters) || 0)
			}
		}
	})
</script>
@endpush
@section('content')
	<header class="page-header">
		<div class="container">
			<ol class="bread">
				<li>
					<a href="{{ route('home') }}"><span><i class="fa fa-home"></i></span></a>
				</li>
				<li class="divider"><span>//</span></li>
				<li>
					<a href="{{ route('transfers', ['from' => $fromLocation->slug, 'to' => $toLocation->slug]) }}"><span>{{ trans('frontend.transfers') }}</span></a>
				</li>
				<li class="divider"><span>//</span></li>
				<li>
					<span>{{ trans('frontend.booking') }}</span>
				</li>
			</ol>
			<h1 style="font-size: 30px;">{{ trans('frontend.booking-trf-from') }} {{ $fromLocation->name }} {{ trans('frontend.to') }} {{ $toLocation->name }}</h1>
		</div>
	</header>

	<div class="container">
		<div class="inner inner-two-col row">
			<div class="col-lg-12">
				@if(session('success') == true)
					<div class="alert alert-yellow">
						<div class="header">
							<span class="fa fa-info-circle"></span>
							{{ trans('frontend.success') }}
						</div>
						<p>{{ session('message') }}</p>
					</div>
				@endif
			</div>
			<div class="col-lg-9">
				<form class="form" method="post" action="{{ route('make-book') }}">
					{{ csrf_field() }}
					<input type="hidden" name="pickup_location_id" value="{{ $fromLocation->id }}">
					<input type="hidden" name="dropoff_location_id" value="{{ $toLocation->id }}">
					<input type="hidden" name="vehicle_id" value="{{ $selectedVehicle->id }}">
					<input type="hidden" name="total" :value="total">
					<input type="hidden" name="currency" :value="transfers[selectedVehicle.id].price.currency">
					<input type="hidden" name="payment_amount" :value="convert(total, transfers[selectedVehicle.id].price.currency, false)">
					<input type="hidden" name="payment_currency" :value="activeCurrency">
					<input type="hidden" name="services" :value="services">
					<span v-if="distance && distance.rows && distance.rows[0] && distance.rows[0].elements[0].duration && distance.rows[0].elements[0].distance && distance.rows[0].elements[0].status !== 'NOT_FOUND'">
					<input type="hidden" name="duration" :value="distance.rows[0].elements[0].duration.value">
					<input type="hidden" name="distance" :value="distance.rows[0].elements[0].distance.value">
					</span>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>{{ trans('a.name-lastname') }} <span class="red">*</span></label>
								<input v-model="form.name" type="text" class="form-control" name="name" placeholder="{{ trans('a.eg-name') }}" required>
								<span class="help-block">
									{{ trans('a.will-printed') }}
								</span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>E-mail <span class="red">*</span></label>
								<input v-model="form.email" type="email" class="form-control" name="email" placeholder="johndoe@example.org" required>
								<span class="help-block">{{ trans('a.email-required-for') }}</span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>{{ trans('a.phone-number') }} <span class="red">*</span></label>
								<input v-model="form.telephone" type="text" class="form-control" name="telephone" placeholder="+1 234 567 89 00" required>
								<span class="help-block">
									{{ trans('a.phone-required-for') }}
								</span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>{{ trans('a.flight-nr') }}</label>
								<input v-model="form.flight" type="text" class="form-control" name="flight" placeholder="{{ trans('a.eg-flight') }}">
								<span class="help-block">{{ trans('a.you-can-inform') }}</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>{{ trans('a.flight-date') }} <span class="red">*</span></label>
								<input v-model="form.flight_date" type="date" class="form-control" name="flight_date" min="{{ date('Y-m-d') }}" required>
								<span class="help-block"></span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>{{ trans('a.flight-time') }}</label>
								<input v-model="form.flight_time" type="time" class="form-control" name="flight_time">
								<span class="help-block"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="has-success">
								<div class="checkbox">
									<label for="has_child_seats">
										<input v-model="form.has_child_seats" id="has_child_seats" type="checkbox" name="has_child_seats">
										{{ trans('a.add-child-seat') }} <span class="label label-success">(<span v-text="convert(5, 'USD')"></span> {{ trans('a.per-seat') }})</span>
									</label>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>{{ trans('a.adults') }} <span class="red">*</span></label>
								<input v-model="form.adults" type="number" class="form-control" name="adults" placeholder="{{ trans('a.adults') }}" min="1" max="16" required>
								<span class="help-block"></span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>{{ trans('a.childs') }}</label>
								<input v-model="form.children" type="number" class="form-control" name="children" placeholder="{{ trans('a.childs') }}" min="0" max="8">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>{{ trans('a.child-seats') }}</label>
								<input v-model="form.child_seats" :disabled="!form.has_child_seats" type="number" class="form-control" name="child_seats" placeholder="{{ trans('a.child-seats') }}" min="0" max="3">
								<span class="help-block">9-18 {{ trans('a.kg') }}</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>{{ trans('a.boosters') }}</label>
								<input v-model="form.boosters" :disabled="!form.has_child_seats" type="number" class="form-control" name="boosters" placeholder="{{ trans('a.boosters') }}" min="0" max="3">
								<span class="help-block">13-36 {{ trans('a.kg') }}</span>
							</div>
						</div>
						<div class="col-md-12">
							<div class="has-success">
								<div class="checkbox">
									<label for="return_transfer">
										<input v-model="form.has_return_transfer" id="return_transfer" type="checkbox" name="return_transfer" value="1">
										{{ trans('a.return-trf') }}
									</label>
								</div>
							</div>
						</div>
						<div class="col-md-6" v-if="form.has_return_transfer">
							<div class="form-group">
								<label>{{ trans('a.return-flight-nr') }}</label>
								<input v-model="form.return_flight" type="text" class="form-control" name="return_flight" placeholder="{{ trans('a.eg-flight') }}">
								<span class="help-block">{{ trans('a.you-can-inform')}}</span>
							</div>
						</div>
						<div class="col-md-3" v-if="form.has_return_transfer">
							<div class="form-group">
								<label>{{ trans('a.return-flight-date') }}</label>
								<input v-model="form.return_flight_date" type="date" class="form-control" name="return_flight_date" min="{{ date('Y-m-d') }}" required>
								<span class="help-block"></span>
							</div>
						</div>
						<div class="col-md-3" v-if="form.has_return_transfer">
							<div class="form-group">
								<label>{{ trans('a.pickup-time') }}</label>
								<input v-model="form.return_flight_time" type="time" class="form-control" name="return_flight_time">
								<span class="help-block">{{ trans('a.before-flight-time') }}</span>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<textarea v-model="form.comments"
										  class="form-control"
										  name="comments"
										  placeholder="{{ trans('a.provide-add-info') }}"></textarea>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
					<hr class="lg" style="margin-top: 5px;margin-bottom: 25px;">
					<h6 style="margin-top: 10px;">{{ trans('a.rez') }}</h6>

					<div class="row" style="border-left: 4px solid #FFC61A;padding-left: 30px; margin-left: 30px;">
						<div class="col-md-12">
							{{ trans('a.paid-driver') }}:
							<span style="color: #093; font-size: 20px; font-weight: bold;" v-text="convert(total, 'USD')"></span>
						</div>
					</div>

					<div class="row">
@if(session('locale') == 'tr')
						<div class="col-md-12">
							<br><br>
	<form method="post" action="{{ route('payment') }}">
		{{ csrf_field() }}
		<tr>
			<td width="100" height="25" bgcolor="#FBFBFB" class="Basliklar">&nbsp;</td>
			<td width="1" bgcolor="#FBFBFB" class="AltCizgi">&nbsp;</td>
			<td bgcolor="#FBFBFB" class="AltCizgi">Kart Bilgileri</td>
		</tr>
		<tr>
			<td width="100" height="25" class="Basliklar">Kart No</td>
			<td width="1" class="AltCizgi">:</td>
			<td class="AltCizgi">
			<input class="input_yeni" type="text" name="KartNo" size="49" value=""></td>
		</tr>
		<tr>
			<td width="100" height="25" class="Basliklar">Son K.Tarihi</td>
			<td width="1" class="AltCizgi"></td>
			<td class="AltCizgi">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="250">
					<input class="input_yeni" type="text" name="KartAy" size="5" value="" placeholder="Ay"></td>
					<td style="padding-left: 5px">
					<input class="input_yeni" type="text" name="KartYil" size="12" value="" placeholder="Yıl"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td width="100" height="25" class="Basliklar">Kart CVV</td>
			<td width="1" class="AltCizgi">:</td>
			<td class="AltCizgi"><input class="input_yeni" type="text" name="KartCvv" size="12" value=""></td>
		</tr>
		<tr>
			<td width="100" height="25" class="Basliklar">&nbsp;</td>
			<td width="1" class="AltCizgi"></td>
		</tr>
	</form>
						</div>
@endif
						<div class="col-md-12 aligncenter">
							<input type="submit" name="send" value="{{ trans('a.finish-book') }}" class="btn btn-yellow btn-lg">
						</div>
						<div class="col-md-12">
							<small>{{ trans('a.by-pressing') }}</small>
						</div>
					</div>
				</form>


			</div>
			<div class="col-lg-3" style="background-color: #fff9e8;">
				<h6 style="margin-top: 10px; font-size: 15px;text-align:center;color: #555;">
					{{ trans('a.summary') }}
				</h6>
				<div class="widget-area" role="complementary" style="background-color: #fff9e8;padding: 10px 20px;">

					<aside class="widget">
						<ul><li class="current-cat" style="font-weight: 300;">
							{{ trans('a.flight_details') }}
						</li></ul>

						<small>
							<span v-text="form.flight"></span><br>
							<span v-text="form.flight_date"></span>
							<span v-text="form.flight_time"></span>
						</small>
						<ul style="margin-top:15px;"><li class="current-cat" style="font-weight: 300;">
							{{ trans('a.pickup') }}
						</li></ul>
						<small v-text="locations[from].name"></small>
						<ul style="margin-top:15px;"><li class="current-cat" style="font-weight: 300;">
							{{ trans('a.dropoff') }}
						</li></ul>
						<small v-text="locations[to].name"></small>
						<ul style="margin-top:15px;"><li class="current-cat" style="font-weight: 300;">
							{{ trans('a.trf-details') }}
						</li></ul>
						<small>
							<span v-if="distance && distance.rows && distance.rows[0] && distance.rows[0].elements[0].duration && distance.rows[0].elements[0].distance && distance.rows[0].elements[0].status !== 'NOT_FOUND'">
							{{ trans('frontend.duration') }}: ~<span v-text="distance.rows[0].elements[0].duration.text"></span><br>
							{{ trans('frontend.distance') }}: ~<span v-text="distance.rows[0].elements[0].distance.text"></span><br>
							</span>
							{{ trans('a.vehicle') }}: <span v-text="selectedVehicle.name"></span> (<span v-text="convert(transfers[selectedVehicle.id].price.price, transfers[selectedVehicle.id].price.currency)"></span>)<br>
							{{ trans('a.passengers') }}: <span v-text="form.adults"></span><span v-text="form.children > 0 ? '+' + form.children : ''"></span>
										<br>
							<span v-if="services.includes('champagne')">
							{{ trans('a.champagne') }}: {{ trans('a.yes') }} (<span v-text="convert(champagnePrice, 'USD')"></span>)
							</span>
							<br>
							<span v-if="services.includes('bouquet')">
							{{ trans('a.bouquet') }}: {{ trans('a.yes') }} (<span v-text="convert(bouquetPrice, 'USD')"></span>)
							</span>
							<br>
							<span v-if="child_seats_count>0">
								{{ trans('a.child-seats') }}:
								<span v-text="child_seats_count"></span>x<span v-text="convert(child_seat_price.price, child_seat_price.currency)"></span>
								 (<span v-text="convert(child_seat_price.price*child_seats_count, 'USD')"></span>)
							<br>
						</small>
						<ul style="margin-top:15px;"><li class="current-cat" style="font-weight: 300;">
							{{ trans('a.total') }}
						</li></ul>
							<h4 style="color: #093;" v-text="convert(total, 'USD')"></h4>
					</aside>
				</div>
			</div>
		</div>
	</div>
@endsection