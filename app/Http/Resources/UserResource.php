<?php namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'name'                      => $this->name,
            'status'                    => $this->status,
            'email'                     => $this->email,
            'created_at'                => $this->created_at,
            'deleted_at'                => $this->deleted_at,
            'phone'                     => $this->phone,
            'banned_at'                 => $this->banned_at,
            'attachments'               => $this->attachments->all()
        ];
    }
}
