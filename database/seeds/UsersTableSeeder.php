<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Bayram',
                'email' => 'bayram4ik@gmail.com',
                'password' => '$2y$10$vhfnJssksbbBQQ.aiEG7leIxf5n79kB.G.SrVAC93KzTtszIjdiUO',
                'remember_token' => 'HOJJe6FR6eZse2byZ35mIQs4CxUGMN3OR8cPUs1HWYZUFX90djG4h3uznA4q',
                'created_at' => '2017-10-25 17:39:12',
                'updated_at' => '2017-12-12 01:15:35',
                'phone' => NULL,
                'banned_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Ahmet Turgut',
                'email' => 'mhm@gmail.com',
                'password' => '$2y$10$vhfnJssksbbBQQ.aiEG7leIxf5n79kB.G.SrVAC93KzTtszIjdiUO',
                'remember_token' => 'HOJJe6FR6eZse2byZ35mIQs4CxUGMN3OR8cPUs1HWYZUFX90djG4h3uznA4q',
                'created_at' => '2017-10-25 17:39:12',
                'updated_at' => '2017-10-25 17:39:12',
                'phone' => '+901234141211',
                'banned_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Marina Visconti',
                'email' => 'mrn@gmail.com',
                'password' => '$2y$10$vhfnJssksbbBQQ.aiEG7leIxf5n79kB.G.SrVAC93KzTtszIjdiUO',
                'remember_token' => 'HOJJe6FR6eZse2byZ35mIQs4CxUGMN3OR8cPUs1HWYZUFX90djG4h3uznA4q',
                'created_at' => '2017-10-25 17:39:12',
                'updated_at' => '2017-10-25 17:39:12',
                'phone' => '+901234141211',
                'banned_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Maksim Afanasiev',
                'email' => 'max@gmail.com',
                'password' => '$2y$10$vhfnJssksbbBQQ.aiEG7leIxf5n79kB.G.SrVAC93KzTtszIjdiUO',
                'remember_token' => 'HOJJe6FR6eZse2byZ35mIQs4CxUGMN3OR8cPUs1HWYZUFX90djG4h3uznA4q',
                'created_at' => '2017-10-25 17:39:12',
                'updated_at' => '2017-10-25 17:39:12',
                'phone' => '+901234141211',
                'banned_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Altan Sihirli',
                'email' => 'gaegae@ad.com',
                'password' => '$2y$10$vhfnJssksbbBQQ.aiEG7leIxf5n79kB.G.SrVAC93KzTtszIjdiUO',
                'remember_token' => 'HOJJe6FR6eZse2byZ35mIQs4CxUGMN3OR8cPUs1HWYZUFX90djG4h3uznA4q',
                'created_at' => '2017-10-25 17:39:12',
                'updated_at' => '2017-10-25 17:39:12',
                'phone' => '+901234141211',
                'banned_at' => NULL,
            ),
        ));
        
        
    }
}