<?php namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Bnb\Laravel\Attachments\HasAttachment;
class User extends Authenticatable{
    use HasApiTokens, Notifiable, HasRoles, HasAttachment;
    protected $casts = ['details' => 'array'];
    protected $fillable = [
        'name',
        'email',
        'password',
        'status'
    ];
    protected $hidden = ['password', 'remember_token'];
}
