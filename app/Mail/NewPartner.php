<?php namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class NewPartner extends Mailable
{
    use Queueable, SerializesModels;

    public $partner;

    public function __construct(User $partner)
    {
        $this->partner = $partner;
    }

    public function build()
    {
        return $this->from('book@adaliatransfer.com')
                    ->view('mails.partners.new');
    }
}
