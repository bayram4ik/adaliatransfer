<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourVariationOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_variation_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_variation_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->boolean('active')->default(true);
            $table->time('auto_stopsale_at')->nullable();
            $table->tinyInteger('default_quota')->unsigned()->default(0);
            $table->tinyInteger('notify_before')->unsigned()->default(1)->comment('Notify operationists n days before tour');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_variation_options');
    }
}
