import Vue 	  from 'vue'
import Router from 'vue-router'

import Dashboard 	from './components/Dashboard.vue'
import Reservations from './components/Reservations'
import Prices	    from './components/Prices'
import Discounts	from './components/Discounts'
import Agencies		from './components/Agencies'
import Customers	from './components/Customers'
import Users 	 	from './components/Users.vue'
import Countries 	from './components/Countries.vue'
import Cities 	 	from './components/Cities.vue'
import Destinations from './components/Destinations.vue'
import SubDestinations from './components/SubDestinations.vue'
import Airports  	from './components/Airports.vue'
import Hotels  		from './components/Hotels.vue'
import Vehicles  	from './components/Vehicles.vue'

Vue.use(Router)

export default new Router({ 
	mode: 'history',
	routes: [
		{ path: '/app', 			 name: 'dashboard', 	component: Dashboard },
		{ path: '/app/reservations', name: 'reservations', 	component: Reservations },
		{ path: '/app/prices', 		 name: 'prices', 		component: Prices },
		{ path: '/app/discounts',    name: 'discounts', 	component: Discounts },
		{ path: '/app/agencies',     name: 'agencies', 		component: Agencies },
		{ path: '/app/customers',    name: 'customers', 	component: Customers },
		{ path: '/app/users', 		 name: 'users', 		component: Users },
		{ path: '/app/countries', 	 name: 'countries', 	component: Countries },
		{ path: '/app/cities', 		 name: 'cities', 		component: Cities },
		{ path: '/app/destinations', 		 name: 'destinations', 		component: Destinations },
		{ path: '/app/subdestinations', 	 name: 'subdestinations', 	component: SubDestinations },
		{ path: '/app/airports', 	 name: 'airports', 		component: Airports },
		{ path: '/app/hotels', 	 	 name: 'hotels', 		component: Hotels },
		{ path: '/app/vehicles', 	 name: 'vehicles', 		component: Vehicles }
	]
})