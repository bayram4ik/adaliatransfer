<?php namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TransformsRequest;

class ConvertStringBooleans extends TransformsRequest
{
    protected function transform($key, $value)
    {
        if($value === 'true' || $value === 'TRUE' || $value === 'on' || $value === 'yes')
            return true;

        if($value === 'false' || $value === 'FALSE' || $value === 'off' || $value === 'no')
            return false;

        return $value;
    }
}